(function() {
  var currentFilter, emailValidate, goAndLoad, html, init, loadMoviesReset, login, menu, modal, movieDetail, movieDetailDiscussion, page, search, setCurrentFilter, _body, _content, _document, _header, _isDesktop, _isMobile, _isTablet, _loader, _log, _window;

  _window = $(window);

  _document = $(document);

  _content = $('#content');

  _header = $('#header');

  _body = $('body');

  currentFilter = null;

  goAndLoad = true;

  page = 2;

  html = $('html').attr('class');

  init = function() {
    search();
    menu();
    modal();
    movieDetailDiscussion();
    login();
    window.addEventListener('popstate', function(e) {
      var currentLocation;
      currentLocation = location.href.split('/');
      currentLocation = currentLocation[currentLocation.length - 1];
      currentLocation = currentLocation === '' ? 'estreias' : currentLocation;
      if ($('.md-modal.md-show').length > 0) {
        $('.md-close').trigger('click');
      }
      return $("html.history .filter ul li a[data-slug='" + currentLocation + "']").click();
    });
    return _window.resize(function() {
      return organize();
    });
  };

  _log = function(obj) {
    return console.log(obj);
  };

  _loader = function(container, append, additionalClass, remove) {
    if (remove === true) {
      return container.find(".loader." + additionalClass).remove();
    } else {
      if (append === true) {
        if (container.find('.loader').length === 0) {
          return container.append($('<div/>').attr({
            'class': "loader " + additionalClass
          }));
        }
      } else {
        return container.html($('<div/>').attr({
          'class': 'loader'
        }));
      }
    }
  };

  _isMobile = function() {
    return _window.width() < 768;
  };

  _isTablet = function() {
    return _window.width() >= 768 && _window.width() < 1025;
  };

  _isDesktop = function() {
    return _window.width() >= 1025;
  };

  search = function() {
    var autocompleteContainer, defaulValue, field, form;
    form = $('#search form');
    field = form.find('input.field');
    defaulValue = field.data('default-value');
    autocompleteContainer = form.find('.auto-complete');
    goAndLoad = true;
    if (!field.val()) {
      field.val(defaulValue);
    }
    field.focus(function() {
      if (field.val() === defaulValue) {
        return field.val('');
      }
    }).blur(function() {
      if (field.val() === '') {
        return field.val(defaulValue);
      }
    });
    if (_isDesktop()) {
      field.keyup(function() {
        var value;
        value = $(this).val();
        if (value.length > 2 && goAndLoad) {
          goAndLoad = false;
          return $.ajax({
            url: "" + basePath + "/busca/" + value + "?ajax=true",
            success: function(data) {
              if (data) {
                autocompleteContainer.html(data).show();
              } else {
                autocompleteContainer.html('').hide();
              }
              return goAndLoad = true;
            }
          });
        } else {
          return autocompleteContainer.html('').hide();
        }
      });
    }
    return form.submit(function() {
      if (field.val() !== '' && field.val() !== defaulValue) {
        location.href = form.attr('action') + '/' + field.val();
      }
      return false;
    });
  };

  menu = function() {
    _document.on('click', 'a.toggle-mobile-menu', function(e) {
      e.preventDefault();
      $(this).toggleClass('active');
      return $('ul.mobile-menu').toggleClass('show');
    });
    return $('ul.menu li a.principal').bind('click', function(e) {
      var $this;
      e.preventDefault();
      $this = $(this);
      $('ul.menu li ul.submenu:not(.hidden)').not($this.parent().find('.submenu')).addClass('hidden');
      return $this.parent().find('.submenu').toggleClass('hidden');
    });
  };

  window.organize = function() {
    var grid;
    grid = $('.grid');
    if (_isMobile() === false) {
      return grid.each(function() {
        var $this, box, column, columns, factor, line, margin, top, totalHeightByColumn;
        $this = $(this);
        line = column = top = 0;
        box = $this.find('.box:eq(0)');
        factor = parseInt($this.width() / box.width());
        margin = Math.round($this.width() - (box.width() * factor));
        totalHeightByColumn = [0, 0, 0, 0];
        $this.find('.box').each(function(i) {
          var boxHeight, prev, _ref;
          if ((i % factor) === 0) {
            line = i / factor;
            column = 0;
          }
          if (line > 0) {
            prev = $this.find(".box[data-grid='" + (line - 1) + ':' + column + "']");
            top = prev.height() + parseInt(prev.css('top').split('px')[0]);
          }
          $(this).css({
            left: (column > 0 ? box.width() * column : 0) + (margin / factor),
            top: top + ((_ref = line === 0) != null ? _ref : {
              0: 20 * line
            })
          }).fadeIn('slow').attr('data-grid', "" + line + ":" + column);
          boxHeight = parseInt($(this).height());
          if (column === 0) {
            totalHeightByColumn[0] += boxHeight;
          } else if (column === 1) {
            totalHeightByColumn[1] += boxHeight;
          } else if (column === 2) {
            totalHeightByColumn[2] += boxHeight;
          } else if (column === 3) {
            totalHeightByColumn[3] += boxHeight;
          }
          return column++;
        });
        $this.height(Math.max.apply(Math, totalHeightByColumn));
        if ($this.hasClass('grid-search')) {
          columns = parseInt($this.find('.box:last').attr('data-grid').split(':')[0]) + 1;
          return $this.css('height', columns * box.height());
        }
      });
    }
  };

  window.filter = function(outOfHome) {
    var $first, area, last, pos;
    $first = $('.filter ul li a.active');
    pos = $first.data('position');
    last = $first.hasClass('last');
    pin(pos, last);
    setCurrentFilter($first.data('slug'));
    if (!outOfHome && _isMobile() === false) {
      $('html.history .filter ul li a').bind('click', function(e) {
        var $this, url;
        e.preventDefault();
        $this = $(this);
        pos = $this.data('position');
        last = $this.hasClass('last');
        url = $this.attr('href');
        pin(pos, last);
        $('.filter ul li a').removeClass('active');
        $this.addClass('active');
        if (currentFilter !== $this.data('slug')) {
          _loader($('.grid'));
          $.ajax({
            url: "" + url + "/true",
            success: function(data) {
              if (data !== 'false') {
                $('.grid').html(data);
              } else {
                $('.grid').html('');
              }
              return organize();
            }
          });
          if (url !== window.location) {
            window.history.pushState({
              path: url
            }, '', url);
          }
          setCurrentFilter($this.data('slug'));
          return loadMoviesReset();
        }
      });
      return area = $('.filter .area');
    }
  };

  window.pin = function(pos, last, checkProximity) {
    var elementPos, i, item, list, _i, _j, _len;
    list = $('.filter ul li a');
    i = 0;
    if (checkProximity === true) {
      for (_i = 0, _len = list.length; _i < _len; _i++) {
        item = list[_i];
        elementPos = parseInt($(list[_i]).data('drag-limit'));
        _j = _i + 1;
        if (pos < elementPos) {
          $(list[_i]).click();
          return false;
        } else if (pos > elementPos && i === (list.length - 2)) {
          $(list[_j]).click();
          return false;
        }
      }
    }
    if ($('html.cssanimations').length >= 1) {
      item = $(".filter ul li a[data-position='" + pos + "']").data('slug');
      $('.filter .pin').attr('class', 'pin');
      $('.filter .bar.continuous').attr('class', 'bar continuous');
      $('.filter .pin').addClass(item);
      return $('.filter .bar.continuous').addClass(item);
    } else {
      $('.filter .pin').animate({
        left: pos
      });
      return $('.filter .bar.continuous').animate({
        width: pos + (last ? 32 : 10)
      });
    }
  };

  setCurrentFilter = function(reference) {
    return currentFilter = reference;
  };

  loadMoviesReset = function() {
    goAndLoad = true;
    return page = 2;
  };

  window.loadMovies = function() {
    var load;
    _document.on('click', 'a.load-more', function(e) {
      var $this;
      e.preventDefault();
      $this = $(this);
      $this.html($this.data('load-text'));
      return load($this);
    });
    if (_isDesktop()) {
      _window.scroll(function() {
        var callback, containerHeight, scrollTop;
        containerHeight = parseInt(_document.height()) - parseInt(_window.height());
        scrollTop = parseInt($(this).scrollTop());
        if (scrollTop >= containerHeight && goAndLoad) {
          goAndLoad = false;
          callback = function() {
            return load();
          };
          return setTimeout(callback, 500);
        }
      });
    }
    return load = function(el) {
      _loader($('body'), true, 'bottom');
      return $.ajax({
        url: "" + basePath + "/" + currentFilter + "/true?page=" + page,
        success: function(data) {
          if (data !== 'false') {
            $('.grid').append(data);
            organize();
            page++;
            goAndLoad = true;
          }
          _loader($('body'), true, 'bottom', true);
          if (el !== void 0) {
            return el.html(el.data('default-text'));
          }
        }
      });
    };
  };

  movieDetail = function(url, callback) {
    return $.ajax({
      type: 'GET',
      dataType: 'json',
      url: url,
      success: function(data) {
        var actor, comment, director, i, movie, template, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2;
        window.history.pushState({
          path: url,
          parent: location.href
        }, '', url);
        movie = $('#md-movie');
        movie.find('h4.name').html(data.name);
        movie.find('h5.original_name').html('( ' + data.original_name + ' - ' + data.year + ' )');
        movie.find('li.release span.text').html(data.release);
        movie.find('li.age span.text').html((data.age === '0' ? 'Livre' : data.age + ' anos'));
        movie.find('li.time span.text').html(data.time + ' min.');
        movie.find('div.poster img').attr({
          'alt': data.name,
          'src': movie.find('div.poster img').data('base-url') + data.poster
        });
        movie.find('p.genre').html(data.genre);
        movie.find('p.review').html(data.review);
        movie.find('p.directors').html('');
        movie.find('p.actors').html('');
        movie.find('.comments .real-comments').html('');
        movie.find('div.rating .text').html(data.rating);
        movie.find('span.rates-count').html('+ ' + data.ratesCount + ' pessoas');
        movie.find('span.favorites-count').html('+ ' + data.favoritesCount + ' pessoas');
        _ref = data.directors;
        for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
          director = _ref[i];
          movie.find('p.directors').append($('<a/>').attr({
            'href': director.url
          }).html(director.name));
          if (i < (data.directors.length - 1)) {
            movie.find('p.directors').append(', ');
          }
        }
        _ref1 = data.actors;
        for (i = _j = 0, _len1 = _ref1.length; _j < _len1; i = ++_j) {
          actor = _ref1[i];
          movie.find('p.actors').append($('<a/>').attr({
            'href': actor.url
          }).html(actor.name));
          if (i < (data.actors.length - 1)) {
            movie.find('p.actors').append(', ');
          }
        }
        movie.find('.comments label').text(movie.find('.comments label').attr('data-comments-text'));
        movie.find('.comments .real-discussion').html('').hide();
        movie.find('.comments .real-discussion').html('').hide();
        movie.find('.box-discussion-comment').hide();
        movie.find('.send-comment').hide().find('textarea').val('');
        movie.find('.comments .real-comments').show();
        _ref2 = data.comments;
        for (i = _k = 0, _len2 = _ref2.length; _k < _len2; i = ++_k) {
          comment = _ref2[i];
          template = movie.find('.comment-model').clone();
          template.find('input[name=rate_id]').val(comment.id);
          template.find('.author-name').html(comment.user_name);
          template.find('.author-date').html(comment.register);
          template.find('.message').html(comment.comment);
          template.find('.like-comment').html(comment.like);
          template.find('.unlike-comment').html(comment.unlike);
          movie.find('.comments .real-comments').append(template.html());
        }
        return callback();
      }
    });
  };

  movieDetailDiscussion = function(url, callback) {
    return _document.on('click', '[rel=reply-comment]', function(e) {
      var containerRealDiscussion, movie, rate_id;
      e.preventDefault();
      rate_id = $(this).prev().val();
      movie = $('#md-movie');
      movie.find('.comments label').text(movie.find('.comments label').attr('data-discussion-text'));
      movie.find('.real-comments').hide();
      movie.find('.send-comment').show();
      containerRealDiscussion = movie.find('.real-discussion');
      containerRealDiscussion.html('').show();
      _loader(containerRealDiscussion, true, 'loader-discussion');
      return $.ajax({
        type: 'POST',
        dataType: 'json',
        data: "rate_id=" + rate_id,
        url: "" + basePath + "/movie/discussion",
        success: function(data) {
          var box_discussion_comment, comment, i, template, _i, _len, _ref, _results;
          _loader(containerRealDiscussion, false, 'loader-discussion', true);
          box_discussion_comment = movie.find('.box-discussion-comment');
          box_discussion_comment.find('.author-name').html(data.user.name);
          box_discussion_comment.find('.author-date').html(data.register);
          box_discussion_comment.find('.message').html(data.comment);
          box_discussion_comment.find('.like-comment').html(data.like);
          box_discussion_comment.find('.unlike-comment').html(data.unlike);
          box_discussion_comment.show();
          _ref = data.discussion;
          _results = [];
          for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
            comment = _ref[i];
            template = movie.find('.comment-model').clone();
            template.find('.author-name').html(comment.user.name);
            template.find('.author-date').html(comment.register);
            template.find('.message').html(comment.comment);
            template.find('.like-comment').remove();
            template.find('.unlike-comment').remove();
            template.find('.reply').remove();
            _results.push(containerRealDiscussion.append(template.html()));
          }
          return _results;
        }
      });
    });
  };

  modal = function() {
    var el, open, removeModal, thisModal;
    el = null;
    thisModal = null;
    if (_isMobile()) {
      return $('.md-overlay, .md-modal').hide();
    } else {
      $(document).on('click', '.md-close, .md-overlay', function(ev) {
        ev.stopPropagation();
        window.history.back(-1);
        return removeModal();
      });
      $(document).keyup(function(ev) {
        if (ev.keyCode === 27) {
          ev.stopPropagation();
          return removeModal();
        }
      });
      removeModal = function(hasPerspective) {
        classie.remove(thisModal, 'md-show');
        _body.removeClass('noScroll');
        $('.md-wrap').hide();
        if (classie.has(el, 'md-setperspective')) {
          return classie.remove(document.documentElement, 'md-perspective');
        }
      };
      open = function() {
        classie.add(thisModal, 'md-show');
        if (classie.has(el, 'md-setperspective')) {
          return setTimeout(function() {
            return classie.add(document.documentElement, 'md-perspective');
          }, 25);
        }
      };
      return $(document).on('click', '.md-trigger', function(ev) {
        var $this;
        ev.preventDefault();
        el = this;
        $this = $(el);
        thisModal = document.getElementById($this.attr('data-modal'));
        if ($this.attr('data-modal') === 'md-movie') {
          return movieDetail($this.attr('href'), function() {
            _body.addClass('noScroll');
            $('.md-wrap.wrap-movie').show().height(_window.height());
            return open();
          });
        } else {
          return open();
        }
      });
    }
  };

  login = function() {
    var form;
    $('a[rel=socialConnect]').bind('click', function(e) {
      return $(this).parent().addClass('loader loading');
    });
    form = $('form[name=login]');
    return form.submit(function() {
      var boxError, email, password;
      email = form.find("[name='User[email]']");
      password = form.find("[name='User[password]']");
      boxError = form.find('div.error');
      boxError.addClass('hide').find('p').html('');
      if (email.val() && password.val()) {
        if (emailValidate(email.val()) === false) {
          boxError.removeClass('hide').find('p').html(email.data('message'));
        } else {
          $.ajax({
            type: 'POST',
            dataType: 'json',
            url: form.attr('action'),
            data: form.serialize(),
            success: function(data) {
              if (data.status === true) {
                return location.reload();
              } else {
                return boxError.removeClass('hide').find('p').html(data.message);
              }
            }
          });
        }
      } else {
        boxError.removeClass('hide').find('p').html(form.data('message'));
      }
      return false;
    });
  };

  window.register = function() {
    return $('form[name=register]').submit(function() {
      var boxError, confirmPassword, email, form, name, password;
      form = $(this);
      name = form.find("[name='User[name]']");
      email = form.find("[name='User[email]']");
      password = form.find("[name='User[password]']");
      confirmPassword = form.find("[name='User[passwordConfirm]']");
      boxError = form.find('div.error');
      boxError.addClass('hide').find('p').html('');
      if (name.val() && email.val() && password.val() && confirmPassword.val()) {
        if (emailValidate(email.val()) === false) {
          boxError.removeClass('hide').find('p').html(email.data('message'));
        } else if (password.val() !== confirmPassword.val()) {
          boxError.removeClass('hide').find('p').html(password.data('message'));
        } else {
          if (form.attr('data-validate-email') === 'true') {
            $.ajax({
              type: 'POST',
              dataType: 'json',
              url: form.attr('action') + '?validate=true',
              data: form.serialize(),
              success: function(status) {
                if (!status) {
                  return boxError.removeClass('hide').find('p').html('Este e-mail já esta sendo usado');
                } else {
                  form.attr('data-validate-email', 'false');
                  return form.submit();
                }
              }
            });
          } else {
            form.find('input[type=submit]').addClass('loader-button');
            return true;
          }
        }
      } else {
        boxError.removeClass('hide').find('p').html(form.data('message'));
      }
      return false;
    });
  };

  emailValidate = function(email) {
    return /\S+@\S+\.\S+/.test(email);
  };

  init();

}).call(this);
