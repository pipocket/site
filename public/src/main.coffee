# global bars
_window = $ window
_document = $ document
_content = $ '#content'
_header = $ '#header'
_body = $ 'body'
currentFilter = null
goAndLoad = true
page = 2
html = $('html').attr 'class'

# Init
init = ->
	search()
	menu()
	modal()
	movieDetailDiscussion()
	login()

	# Popstate
	window.addEventListener 'popstate', (e) ->
		currentLocation = location.href.split('/')
		currentLocation = currentLocation[currentLocation.length-1]
		currentLocation = if currentLocation is '' then 'estreias' else currentLocation

		if $('.md-modal.md-show').length > 0
			$('.md-close').trigger 'click'

		$("html.history .filter ul li a[data-slug='#{currentLocation}']").click()

	# resize window
	_window.resize ->
		organize()

# Helpers
_log = (obj) ->
	console.log obj

_loader = (container, append, additionalClass, remove) ->
	if remove is true
		container.find(".loader.#{additionalClass}").remove()
	else
		if append is true
			if container.find('.loader').length == 0
				container.append $('<div/>').attr({'class':"loader #{additionalClass}"})
		else
			container.html $('<div/>').attr({'class':'loader'})

_isMobile = ->
	return (_window.width() < 768)

_isTablet = ->
	return (_window.width() >= 768 and _window.width() < 1025)

_isDesktop = ->
	return (_window.width() >= 1025)

# Functions
search = ->
	form = $ '#search form'
	field = form.find 'input.field'
	defaulValue = field.data 'default-value';
	autocompleteContainer = form.find '.auto-complete'
	goAndLoad = true

	if !field.val()
		field.val defaulValue

	field
		.focus ->
			if field.val() is defaulValue
				field.val ''
		.blur ->
			if field.val() is ''
				field.val defaulValue

	# Auto-complete
	if _isDesktop()
		field.keyup ->
			value = $(this).val()

			if value.length > 2 and goAndLoad
				goAndLoad = false

				# find
				$.ajax
					url: "#{basePath}/busca/#{value}?ajax=true",
					success: (data) ->
						if data
							autocompleteContainer.html(data).show()
						else
							autocompleteContainer.html('').hide()

						# free to load
						goAndLoad = true
			else
				autocompleteContainer.html('').hide()

	form.submit ->
		if field.val() isnt '' and field.val() isnt defaulValue
			location.href = form.attr('action') + '/' + field.val()

		return false

menu = ->
	_document.on 'click', 'a.toggle-mobile-menu', (e) ->
		e.preventDefault()

		$(this).toggleClass 'active'
		$('ul.mobile-menu').toggleClass 'show'

	$('ul.menu li a.principal').bind 'click', (e) ->
		e.preventDefault()
		$this = $ this
		
		$('ul.menu li ul.submenu:not(.hidden)').not($this.parent().find('.submenu')).addClass 'hidden'
		$this.parent().find('.submenu').toggleClass 'hidden'

# public function
window.organize = ->
	grid = $ '.grid'

	if _isMobile() is false
		grid.each ->
			$this = $ this
			line = column = top = 0
			box = $this.find '.box:eq(0)'
			factor = parseInt $this.width()/box.width()
			margin = Math.round ($this.width()-(box.width()*factor))
			totalHeightByColumn = [0,0,0,0]

			$this.find('.box').each (i) ->
				if (i%factor) == 0
					line = (i/factor)
					column = 0

				if line > 0
					prev = $this.find ".box[data-grid='"+(line-1)+':'+column+"']"
					top = prev.height()+parseInt(prev.css('top').split('px')[0])

				$(this)
					.css
						left: (if column>0 then (box.width()*column) else 0) + (margin/factor),
						top: top+(line==0 ? 0 : (20*line))
					.fadeIn('slow').attr 'data-grid', "#{line}:#{column}"

				# Set column height
				boxHeight = parseInt $(this).height()

				if column is 0
					totalHeightByColumn[0] += boxHeight
				else if column is 1
					totalHeightByColumn[1] += boxHeight
				else if column is 2
					totalHeightByColumn[2] += boxHeight
				else if column is 3
					totalHeightByColumn[3] += boxHeight

				column++

			# Set grid height
			$this.height Math.max.apply( Math, totalHeightByColumn )

			if $this.hasClass('grid-search')
				columns = parseInt($this.find('.box:last').attr('data-grid').split(':')[0]) + 1
				$this.css 'height', columns*box.height()

# public function
window.filter = (outOfHome) ->
	# First time
	$first = $ '.filter ul li a.active'
	pos = $first.data 'position'
	last = $first.hasClass 'last'
	
	# Move pin
	pin pos, last

	# Register current filter
	setCurrentFilter $first.data('slug')

	if !outOfHome and _isMobile() is false
		# Click filter
		$('html.history .filter ul li a').bind 'click', (e) ->
			e.preventDefault()
			$this = $ this
			pos = $this.data 'position'
			last = $this.hasClass 'last'
			url = $this.attr 'href'

			# Move pin
			pin pos, last

			$('.filter ul li a').removeClass 'active'
			$this.addClass 'active'

			if currentFilter != $this.data('slug')
				# Call loader
				_loader $('.grid')

				# Call movie list
				$.ajax
					url: "#{url}/true",
					success: (data) ->

						if data != 'false'
							$('.grid').html data
						else
							$('.grid').html ''

						organize()

				# Change url
				if url != window.location
					window.history.pushState({path:url}, '', url);

				# Register current filter
				setCurrentFilter $this.data('slug')

				# Free to load more movies
				loadMoviesReset()

		area = $ '.filter .area'

window.pin = (pos, last, checkProximity) ->
	list = $ '.filter ul li a'
	i = 0

	if checkProximity is true
		for item in list
			elementPos = parseInt $(list[_i]).data 'drag-limit'
			_j = _i+1

			if pos < elementPos
				$(list[_i]).click()
				return false
			else if pos > elementPos and i==(list.length-2)
				$(list[_j]).click()
				return false

	if $('html.cssanimations').length >= 1
		# get current item
		item = $(".filter ul li a[data-position='#{pos}']").data('slug')

		# clear
		$('.filter .pin').attr 'class', 'pin'
		$('.filter .bar.continuous').attr 'class', 'bar continuous'

		# add class
		$('.filter .pin').addClass item
		$('.filter .bar.continuous').addClass item
	else
		$('.filter .pin').animate
			left: pos

		$('.filter .bar.continuous').animate
			width: pos+(if last then 32 else 10)

setCurrentFilter = (reference) ->
	currentFilter = reference

loadMoviesReset = ->
	goAndLoad = true
	page = 2

window.loadMovies = ->
	# Tablet & Mobile
	_document.on 'click', 'a.load-more', (e) ->
		e.preventDefault()
		$this = $ this

		$this.html $this.data('load-text')
		load($this);

	# Desktop
	if _isDesktop()
		_window.scroll ->
			containerHeight = parseInt(_document.height()) - parseInt(_window.height())
			scrollTop = parseInt($(this).scrollTop())

			if scrollTop >= containerHeight and goAndLoad
				goAndLoad = false

				callback = ->
					load()

				# Awaiting 0,5s to load
				setTimeout callback, 500

	# Load
	load = (el) ->
		# show loader
		_loader $('body'), true, 'bottom'

		# get more movies
		$.ajax
			url: "#{basePath}/#{currentFilter}/true?page=#{page}",
			success: (data) ->
				if data isnt 'false'
					# add to grid
					$('.grid').append data

					# organize movie's box
					organize()

					# prepare to next load
					page++

					# free to load
					goAndLoad = true

				# hide loader
				_loader $('body'), true, 'bottom', true

				# Change element text
				if el isnt undefined
					el.html el.data('default-text')

movieDetail = (url, callback) ->
	$.ajax
		type: 'GET',
		dataType: 'json',
		url: url,
		success: (data) ->
			# Change url
			window.history.pushState({path:url, parent:location.href}, '', url);

			movie = $('#md-movie')
			movie.find('h4.name').html data.name
			movie.find('h5.original_name').html '( ' + data.original_name  + ' - ' + data.year + ' )'
			movie.find('li.release span.text').html data.release
			movie.find('li.age span.text').html (if data.age is '0' then 'Livre' else data.age + ' anos')
			movie.find('li.time span.text').html data.time + ' min.'
			movie.find('div.poster img').attr( {'alt': data.name, 'src': movie.find('div.poster img').data('base-url') + data.poster} );
			movie.find('p.genre').html data.genre
			movie.find('p.review').html data.review
			movie.find('p.directors').html ''
			movie.find('p.actors').html ''
			movie.find('.comments .real-comments').html ''
			movie.find('div.rating .text').html data.rating
			movie.find('span.rates-count').html '+ ' + data.ratesCount + ' pessoas'
			movie.find('span.favorites-count').html '+ ' + data.favoritesCount + ' pessoas'

			for director, i in data.directors
				movie.find('p.directors').append $('<a/>').attr( {'href':director.url} ).html(director.name)

				if i < (data.directors.length-1)
					movie.find('p.directors').append ', '
 
			for actor, i in data.actors
				movie.find('p.actors').append $('<a/>').attr({'href':actor.url}).html(actor.name)

				if i < (data.actors.length-1)
					movie.find('p.actors').append ', '

			movie.find('.comments label').text movie.find('.comments label').attr('data-comments-text')
			movie.find('.comments .real-discussion').html('').hide()
			movie.find('.comments .real-discussion').html('').hide()
			movie.find('.box-discussion-comment').hide()
			movie.find('.send-comment').hide().find('textarea').val ''
			movie.find('.comments .real-comments').show()

			for comment, i in data.comments
				template = movie.find('.comment-model').clone()
				template.find('input[name=rate_id]').val comment.id
				template.find('.author-name').html comment.user_name
				template.find('.author-date').html comment.register
				template.find('.message').html comment.comment
				template.find('.like-comment').html comment.like
				template.find('.unlike-comment').html comment.unlike

				movie.find('.comments .real-comments').append template.html()

			callback()

movieDetailDiscussion = (url, callback) ->
	_document.on 'click', '[rel=reply-comment]', (e) ->
		e.preventDefault()

		rate_id = $(this).prev().val()

		movie = $('#md-movie')
		movie.find('.comments label').text movie.find('.comments label').attr('data-discussion-text')
		movie.find('.real-comments').hide()
		movie.find('.send-comment').show()

		containerRealDiscussion = movie.find('.real-discussion')
		containerRealDiscussion.html('').show()

		_loader(containerRealDiscussion, true, 'loader-discussion')

		$.ajax
			type: 'POST',
			dataType: 'json',
			data: "rate_id=#{rate_id}",
			url: "#{basePath}/movie/discussion",
			success: (data) ->
				_loader(containerRealDiscussion, false, 'loader-discussion', true)

				box_discussion_comment = movie.find('.box-discussion-comment')
				box_discussion_comment.find('.author-name').html data.user.name
				box_discussion_comment.find('.author-date').html data.register
				box_discussion_comment.find('.message').html data.comment
				box_discussion_comment.find('.like-comment').html data.like
				box_discussion_comment.find('.unlike-comment').html data.unlike
				box_discussion_comment.show()

				for comment, i in data.discussion
					template = movie.find('.comment-model').clone()
					template.find('.author-name').html comment.user.name
					template.find('.author-date').html comment.register
					template.find('.message').html comment.comment
					template.find('.like-comment').remove()
					template.find('.unlike-comment').remove()
					template.find('.reply').remove()

					containerRealDiscussion.append template.html()

modal = () ->
	el = null
	thisModal = null

	if _isMobile()
		$('.md-overlay, .md-modal').hide()
	else
		# close modal
		$(document).on 'click', '.md-close, .md-overlay', ( ev ) ->
			ev.stopPropagation()

			# Back to home
			window.history.back -1

			removeModal()

		# close modal when ESC is pressed
		$(document).keyup (ev) ->
			if ev.keyCode is 27
				ev.stopPropagation()
				removeModal()

		# Remove modal function
		removeModal = ( hasPerspective ) ->
			classie.remove( thisModal, 'md-show' );

			_body.removeClass 'noScroll'
			$('.md-wrap').hide()

			if classie.has( el, 'md-setperspective' )
				classie.remove document.documentElement, 'md-perspective'

		open = () ->
			classie.add( thisModal, 'md-show' )

			if classie.has( el, 'md-setperspective' )
				setTimeout ->
					classie.add( document.documentElement, 'md-perspective' )
				, 25

		# open modal
		$(document).on 'click', '.md-trigger', ( ev ) ->
			ev.preventDefault()

			el = this
			$this = $ el
			thisModal = document.getElementById $this.attr('data-modal')

			if $this.attr('data-modal') == 'md-movie'
				movieDetail($this.attr('href'), ->
					_body.addClass 'noScroll'
					$('.md-wrap.wrap-movie').show().height _window.height()
					open()
				)
			else
				open()

login = ->
	# Social Connect
	$('a[rel=socialConnect]').bind 'click', (e) ->
		$(this).parent().addClass 'loader loading'

	form = $ 'form[name=login]'
	form.submit ->
		email = form.find "[name='User[email]']"
		password = form.find "[name='User[password]']"
		boxError = form.find 'div.error'

		# remove all errors
		boxError.addClass('hide').find('p').html ''

		if email.val() and password.val()
			if emailValidate(email.val()) is false
				boxError.removeClass('hide').find('p').html email.data('message')
			else
				# login
				$.ajax
					type: 'POST',
					dataType: 'json',
					url: form.attr('action'),
					data: form.serialize(),
					success: (data) ->
						if data.status is true
							location.reload()
						else
							boxError.removeClass('hide').find('p').html data.message
		else
			boxError.removeClass('hide').find('p').html form.data('message')

		return false

window.register = ->
	$('form[name=register]').submit ->
		form = $ this
		name = form.find "[name='User[name]']"
		email = form.find "[name='User[email]']"
		password = form.find "[name='User[password]']"
		confirmPassword = form.find "[name='User[passwordConfirm]']"
		boxError = form.find 'div.error'

		# remove all errors
		boxError.addClass('hide').find('p').html ''

		if name.val() and email.val() and password.val() and confirmPassword.val()
			if emailValidate( email.val() ) is false
				boxError.removeClass('hide').find('p').html email.data('message')
			else if password.val() isnt confirmPassword.val()
				boxError.removeClass('hide').find('p').html password.data('message')
			else
				if form.attr('data-validate-email') == 'true'
					$.ajax
						type: 'POST',
						dataType: 'json',
						url: form.attr('action') + '?validate=true',
						data: form.serialize(),
						success: (status) ->
							if !status
								boxError.removeClass('hide').find('p').html 'Este e-mail já esta sendo usado'
							else
								form.attr 'data-validate-email', 'false'
								form.submit()
				else
					form.find('input[type=submit]').addClass 'loader-button'
					return true
		else
			boxError.removeClass('hide').find('p').html form.data('message')

		return false

emailValidate = (email) ->
	return /\S+@\S+\.\S+/.test(email)

# Call default funcions
init()