<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

// Local config file
$local_config_file = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'main-local.php';

// Config
$main = array(
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'Pipocket',
	'defaultController' => 'home',

	// preloading 'log' component
	'preload' => array('log'),

	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.extensions.*',
		'application.extensions.aws.Aws',
	),

	'modules' => array(
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => 'simples',
			'ipFilters' => array('127.0.0.1','::1'),
		),
	),

	// application components
	'components' => array(
		'user' => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
		),
		'urlManager' => array(
			'urlFormat' => 'path',
			'caseSensitive' => false,
			'showScriptName' => false,
			'rules' => array(
				'sobre' => 'about/index',
				'sobre/politica-de-privacidade' => 'about/privacy',
				'sobre/termos-de-uso' => 'about/terms',
				'contato' => 'contact/index',
				'filme/<slug>' => 'movie/index',
				'busca/<term>' => 'search/index',
				'logout' => 'login/exit',
				'cadastro' => 'register/index',
				'cadastro/ativar' => 'register/activate',
				'cadastro/email-ativacao' => 'register/activationMail',
				'login/esqueci-minha-senha' => 'login/forgotpassword',
				'login/redefinir-senha' => 'login/resetpassword',
				'<filter:(estreias|melhores-avaliados|mais-vistos|em-cartaz|todos)>' => 'home/index',
				'<filter>/<ajax:(true)>' => 'home/movies',
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
		),
		'db' => array(
			'connectionString'  =>  'mysql:host=127.0.0.1;dbname=pipocket',
			'emulatePrepare'  =>  true,
			'username'  =>  'root',
			'password'  =>  'ppckt!06#13',
			'charset'  =>  'utf8',
		),
		'errorHandler' => array(
			'errorAction' => 'home/error',
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),

	// using Yii::app()->params['paramName']
	'params' => array(
		// Amazon Web Service
		'aws' => array(
			's3BaseUrl' => 'https://s3-sa-east-1.amazonaws.com/pipocket/',
			'key' => 'AKIAIMB53NYHYUWHCH2Q',
			'secret' => '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',
		),
		'social' => array(
			'facebook' => array(
				'appId'   =>  '618098408229555',
				'secret'  =>  'be9efd67972bb011834901342cc22c58'
			),
			'twitter' => array(
				'consumerKey'   =>  'R57M5Tj9AEO4fL0AJdcJw',
				'consumerSecret'  =>  'KzFe4j7bikV0977Yx8fLLGmnXQYBHXYtttG9fMMfX8',
				'oauthCallback'  =>  '/login/twitter'
			),
			'google' => array(
				'applicationName'  =>  'Pipocket Dev',
				'clientId'  =>  '841551773909-r6h2f92kdk8mq2d6f6uuov1pdbsdvqq0.apps.googleusercontent.com',
				'clientSecret'  =>  'o_txT5hzo2WI5kyk2kmzJeaW',
				'redirectUri'  =>  '/login/googleplus'
			),
		),
		'securityResetPasswordHash' => '@.',
		'adminEmail' => 'webmaster@pipocket.com',
		'basePath' => dirname(dirname(dirname(__FILE__))),
		'analytics' => false,
		'paginationLimit' => 12,
		'loginDuration' => (3600*24*7), // 7 Days,
		'env' => (($_SERVER['HTTP_HOST'] == 'pipocket.com' OR $_SERVER['HTTP_HOST'] == 'www.pipocket.com') ? 'prod' : 'dev'),
	),
);

if ( file_exists($local_config_file) )
	require $local_config_file;

return $main;