<?php

/**
 * This is the model class for table "user_social".
 *
 * The followings are the available columns in table 'user_social':
 * @property integer $id
 * @property integer $user_id
 * @property string $network
 * @property string $network_id
 * @property string $access_token
 * @property string $register
 *
 * The followings are the available model relations:
 * @property User $user
 */
class UserSocial extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserSocial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_social';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, network, network_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('network, network_id', 'length', 'max'=>125),
			array('register', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, network, network_id, access_token, register', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'network' => 'Network',
			'network_id' => 'Network',
			'access_token' => 'Access Token',
			'register' => 'Register',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('network',$this->network,true);
		$criteria->compare('network_id',$this->network_id,true);
		$criteria->compare('access_token',$this->access_token,true);
		$criteria->compare('register',$this->register,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function checkAndSave($userId, $network, $networkId, $accessToken)
	{
		// Find
		$model = self::model()->find('user_id=:user_id AND network=:network', array(':user_id'=>$userId, ':network'=>$network));

		// Create
		if (!$model) {
			$model = new UserSocial;
			$model->user_id = $userId;
			$model->network = $network;
			$model->network_id = $networkId;
		}

		// Update
		if ($network!='twitter')
			$model->access_token = $accessToken;

		return $model->save();
	}
}