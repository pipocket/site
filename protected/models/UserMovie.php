<?php

/**
 * This is the model class for table "user_movie".
 *
 * The followings are the available columns in table 'user_movie':
 * @property integer $user_id
 * @property integer $movie_id
 * @property integer $view
 * @property string $view_register
 * @property integer $favorite
 * @property string $favorite_register
 * @property integer $like
 * @property string $like_register
 */
class UserMovie extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserMovie the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_movie';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, movie_id', 'required'),
			array('user_id, movie_id, view, favorite, like', 'numerical', 'integerOnly'=>true),
			array('view_register, favorite_register, like_register', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, movie_id, view, view_register, favorite, favorite_register, like, like_register', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function scopes()
	{
		return array(
			'favorited'=>array('condition'=>'favorite = 1')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'movie_id' => 'Movie',
			'view' => 'View',
			'view_register' => 'View Register',
			'favorite' => 'Favorite',
			'favorite_register' => 'Favorite Register',
			'like' => 'Like',
			'like_register' => 'Like Register',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('movie_id',$this->movie_id);
		$criteria->compare('view',$this->view);
		$criteria->compare('view_register',$this->view_register,true);
		$criteria->compare('favorite',$this->favorite);
		$criteria->compare('favorite_register',$this->favorite_register,true);
		$criteria->compare('like',$this->like);
		$criteria->compare('like_register',$this->like_register,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}