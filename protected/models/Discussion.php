<?php

/**
 * This is the model class for table "discussion".
 *
 * The followings are the available columns in table 'discussion':
 * @property integer $id
 * @property string $comment
 * @property integer $user_id
 * @property integer $rate_id
 * @property string $register
 * @property integer $notification
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Rate $rate
 * @property User $user
 */
class Discussion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Discussion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'discussion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, rate_id, register', 'required'),
			array('user_id, rate_id, notification, status', 'numerical', 'integerOnly'=>true),
			array('comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, comment, user_id, rate_id, register, notification, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rate' => array(self::BELONGS_TO, 'Rate', 'rate_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'comment' => 'Comment',
			'user_id' => 'User',
			'rate_id' => 'Rate',
			'register' => 'Register',
			'notification' => 'Notification',
			'status' => 'Status',
		);
	}

	public function defaultScope()
	{
		$scope = array(
			'condition'=>'status=1'
		);

		return $scope;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('rate_id',$this->rate_id);
		$criteria->compare('register',$this->register,true);
		$criteria->compare('notification',$this->notification);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function byComment($rate_id)
	{
		$discussion = $return = array();
		$criteria = new CDbCriteria;
		$criteria->condition = 'rate_id = :rate_id';
		$criteria->params = array(':rate_id' => $rate_id);
		$criteria->limit = 5;

		$rate = Rate::model()->findByPk($rate_id);

		if ($rate) {
			$data = self::model()->findAll($criteria);

			foreach ($data as $comment) {
				$discussion[] = array(
					'id' => $comment->id,
					'comment' => $comment->comment,
					'register' => Yii::app()->dateFormatter->format('dd/MM/y', strtotime($comment->register)),
					'user' => array(
						'id' => $comment->user->id,
						'name' => $comment->user->name,
					)
				);
			}

			// Rate + Discuttion
			$return = array(
				'comment' => $rate->comment,
				'like' => $rate->like,
				'unlike' => $rate->unlike,
				'register' => Yii::app()->dateFormatter->format('dd/MM/y', strtotime($rate->register)),
				'user' => array(
					'id' => $rate->user->id,
					'name' => $rate->user->name,
				),
				'discussion' => $discussion
			);
		}

		return (count($return)==0 ? null : $return);
	}
}