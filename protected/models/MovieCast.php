<?php

/**
 * This is the model class for table "movie_cast".
 *
 * The followings are the available columns in table 'movie_cast':
 * @property integer $movie_id
 * @property integer $cast_id
 * @property integer $actor
 * @property integer $director
 */
class MovieCast extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MovieCast the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'movie_cast';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('movie_id, cast_id', 'required'),
			array('movie_id, cast_id, actor, director', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('movie_id, cast_id, actor, director', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cast'=>array(self::BELONGS_TO, 'Cast', 'cast_id')
		);
	}

	public function scopes()
	{
		return array(
			'actors'=>array('condition'=>'actor=1'),
			'directors'=>array('condition'=>'director=1'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'movie_id' => 'Movie',
			'cast_id' => 'Cast',
			'actor' => 'Actor',
			'director' => 'Director',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('movie_id',$this->movie_id);
		$criteria->compare('cast_id',$this->cast_id);
		$criteria->compare('actor',$this->actor);
		$criteria->compare('director',$this->director);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}