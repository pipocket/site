<?php
class Common extends CActiveRecord
{
	public function search()
	{
		$item = array();
		$term = Yii::app()->request->getParam('term');

		$criteria = new CDbCriteria;
		$criteria->select = 'id, name, year';
		$criteria->condition = 'name LIKE :name OR original_name LIKE :original_name';
		$criteria->params = array(':name'=>"%{$term}%", ':original_name'=>"%{$term}%");
		$criteria->order = 'name';
		$criteria->limit = 2;

		$data = self::model()->findAll($criteria);

		foreach($data as $item) {
			$items[] = array('id'=>$movie->id, 'name'=>$movie->name, 'year'=>$movie->year, 'type'=>'movie');
		}

		return $items;
	}
}