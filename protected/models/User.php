<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $gender
 * @property string $birthdate
 * @property string $avatar
 * @property string $register
 * @property integer $status
 * @property integer $temp_old_id
 *
 * The followings are the available model relations:
 * @property Contact[] $contacts
 * @property Discussion[] $discussions
 * @property Rate[] $rates
 * @property Cast[] $casts
 * @property Genre[] $genres
 * @property UserLog[] $userLogs
 * @property Movie[] $movies
 */
class User extends CActiveRecord
{
	public $passwordConfirm;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			// array('password', 'required', 'on'=>'formRegister'),
			array('status, temp_old_id', 'numerical', 'integerOnly'=>true),
			array('name, username, email, city, state, country', 'length', 'max'=>125),
			array('avatar', 'length', 'max'=>255),
			array('password', 'length', 'max'=>60, 'min'=>6),
			array('gender', 'length', 'max'=>15),
			array('birthdate', 'safe'),
			// array('passwordConfirm', 'compare', 'compareAttribute'=>'password', 'on'=>'formRegister'),
			array('passwordConfirm', 'compare', 'compareAttribute'=>'password', 'on'=>'formResetPassword'),
			array('email', 'email'),
			array('register', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, username, email, password, city, state, country, gender, birthdate, avatar, register, status, temp_old_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contacts' => array(self::HAS_MANY, 'Contact', 'user_id'),
			'discussions' => array(self::HAS_MANY, 'Discussion', 'user_id'),
			'rates' => array(self::HAS_MANY, 'Rate', 'user_id'),
			'casts' => array(self::MANY_MANY, 'Cast', 'user_cast(user_id, cast_id)'),
			'genres' => array(self::MANY_MANY, 'Genre', 'user_genre(user_id, genre_id)'),
			'userLogs' => array(self::HAS_MANY, 'UserLog', 'user_id'),
			'movies' => array(self::MANY_MANY, 'Movie', 'user_movie(user_id, movie_id)'),
			'social' => array(self::HAS_MANY, 'UserSocial', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'username' => 'Username',
			'email' => 'Email',
			'password' => 'Password',
			'city' => 'City',
			'state' => 'State',
			'country' => 'Country',
			'gender' => 'Gender',
			'birthdate' => 'Birthdate',
			'avatar' => 'Avatar',
			'register' => 'Register',
			'status' => 'Status',
			'temp_old_id' => 'Temp Old',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('birthdate',$this->birthdate,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('register',$this->register,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('temp_old_id',$this->temp_old_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave()
	{
		if($this->isNewRecord OR $this->scenario=='formResetPassword') {
			$this->password = Bcrypt::hash($this->password);
		}

		return parent::beforeSave();
	}

	public function checkAndSave($data, $network, $networkId, $accessToken)
	{
		// Convert to object
		$data = is_object($data) ? $data : (object) $data;

		// Find
		if ($network=='twitter' OR $network=='googleplus') {
			$criteria = new CDbCriteria;
			$criteria->compare('network', $network);
			$criteria->compare('network_id', $networkId);
			$model = self::model()->with('social')->find($criteria);
		}
		// If facebook user has Pipocket account, merge both
		else {
			$model = self::model()->find('email=:email', array(':email'=>$data->email));
		}

		// New user
		if (!$model) {
			$model = new User;
		}

		// Update/Insert data
		if ($network=='facebook') {
			$model->email = $data->email;
			$model->name = $data->first_name . ' ' . $data->last_name;
			$model->avatar = $data->photo;
		}
		else if ($network=='twitter') {
			$model->name = $data->name;
			$model->avatar = $data->profile_image_url;
		}
		else if ($network=='googleplus') {
			$model->name = $data->displayName;
			$model->avatar = $data->image['url'];
		}

		// Set gender
		if ($network=='facebook' OR $network=='googleplus') {
			if ($data->gender == 'male') {
				$model->gender = 'M';
			}
			else if ($data->gender == 'female') {
				$model->gender = 'F';
			}
		}

		// Set status active
		$model->status = 1;

		// Save
		$status = $model->save();

		// Save network data
		if ($status) {
			UserSocial::model()->checkAndSave($model->id, $network, $networkId, $accessToken);
		}

		return $status;
	}
}