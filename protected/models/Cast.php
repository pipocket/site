<?php

/**
 * This is the model class for table "cast".
 *
 * The followings are the available columns in table 'cast':
 * @property integer $id
 * @property string $name
 * @property string $original_name
 * @property string $slug
 * @property string $about
 * @property string $birthday
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $photo
 * @property integer $actor
 * @property integer $director
 * @property string $register
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Movie[] $movies
 * @property User[] $users
 */
class Cast extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Cast the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cast';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, register', 'required'),
			array('actor, director, status', 'numerical', 'integerOnly'=>true),
			array('name, original_name, slug, city, state, country, photo', 'length', 'max'=>125),
			array('about, birthday', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, original_name, slug, about, birthday, city, state, country, photo, actor, director, register, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'movies' => array(self::MANY_MANY, 'Movie', 'movie_cast(cast_id, movie_id)'),
			'users' => array(self::MANY_MANY, 'User', 'user_cast(cast_id, user_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'original_name' => 'Original Name',
			'slug' => 'Slug',
			'about' => 'About',
			'birthday' => 'Birthday',
			'city' => 'City',
			'state' => 'State',
			'country' => 'Country',
			'photo' => 'Photo',
			'actor' => 'Actor',
			'director' => 'Director',
			'register' => 'Register',
			'status' => 'Status',
		);
	}

	public function search()
	{
		$cast = array();
		$term = Yii::app()->request->getParam('term');
		$ajax = (Yii::app()->request->getParam('ajax')=='true');

		if ($ajax)
			$limit = 1;
		else
			$limit = 6;

		$criteria = new CDbCriteria;
		$criteria->select = 'id, name, photo, actor, director, slug';
		$criteria->condition = 'name LIKE :name OR original_name LIKE :original_name';
		$criteria->params = array(':name'=>"%{$term}%", ':original_name'=>"%{$term}%");
		$criteria->order = 'name';
		$criteria->limit = $limit;

		$data = self::model()->findAll($criteria);

		if ($ajax) {
			foreach($data as $person) {
				if ($person->actor==1 AND $person->director==1)
					$additional = 'Ator e Diretor';
				else if ($person->actor==1)
					$additional = 'Ator';
				else if ($person->director==1)
					$additional = 'Diretor';

				$cast[] = array('id'=>$person->id, 'name'=>$person->name, 'image'=>$person->photo, 'slug'=>$person->slug, 'additional'=>$additional);
			}

			return $cast;
		}
		else
			return $data;
	}
}