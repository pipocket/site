<?php

/**
 * This is the model class for table "rate".
 *
 * The followings are the available columns in table 'rate':
 * @property integer $id
 * @property integer $rating
 * @property integer $user_id
 * @property integer $movie_id
 * @property string $comment
 * @property integer $like
 * @property integer $unlike
 * @property string $register
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Discussion[] $discussions
 * @property Movie $movie
 * @property User $user
 */
class Rate extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Rate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rating, user_id, movie_id, register', 'required'),
			array('rating, user_id, movie_id, like, unlike, status', 'numerical', 'integerOnly'=>true),
			array('comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, rating, user_id, movie_id, comment, like, unlike, register, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'discussions' => array(self::HAS_MANY, 'Discussion', 'rate_id'),
			'movie' => array(self::BELONGS_TO, 'Movie', 'movie_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rating' => 'Rating',
			'user_id' => 'User',
			'movie_id' => 'Movie',
			'comment' => 'Comment',
			'like' => 'Like',
			'unlike' => 'Unlike',
			'register' => 'Register',
			'status' => 'Status',
		);
	}

	public function scopes()
	{
		return array(
			'commented'=>array('condition'=>'comment IS NOT NULL')
			);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('movie_id',$this->movie_id);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('like',$this->like);
		$criteria->compare('unlike',$this->unlike);
		$criteria->compare('register',$this->register,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}