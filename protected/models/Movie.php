<?php

/**
 * This is the model class for table "movie".
 *
 * The followings are the available columns in table 'movie':
 * @property integer $id
 * @property string $name
 * @property string $original_name
 * @property string $slug
 * @property integer $age
 * @property integer $time
 * @property integer $year
 * @property string $review
 * @property integer $review_status
 * @property string $release
 * @property string $poster
 * @property string $rating
 * @property string $evaluated
 * @property string $forited
 * @property string $commented
 * @property string $anticipated
 * @property string $register
 * @property string $published_in
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Cast[] $casts
 * @property Country[] $countries
 * @property Genre[] $genres
 * @property Rate[] $rates
 * @property User[] $users
 */
class Movie extends CActiveRecord
{
	public $boxLenght;
	public $comment;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Movie the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'movie';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, original_name, slug, year, review, register', 'required'),
			array('age, time, year, review_status, status', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('original_name, slug, poster', 'length', 'max'=>125),
			array('release, published_in', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, original_name, slug, age, time, year, review, review_status, release, poster, register, published_in, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'directors' => array(self::HAS_MANY, 'MovieCast', 'movie_id', 'scopes'=>array('directors')),
			'actors' => array(self::HAS_MANY, 'MovieCast', 'movie_id', 'scopes'=>array('actors')),
			'countries' => array(self::MANY_MANY, 'Country', 'movie_country(movie_id, country_id)'),
			'genres' => array(self::MANY_MANY, 'Genre', 'movie_genre(movie_id, genre_id)'),
			'rates' => array(self::HAS_MANY, 'Rate', 'movie_id', 'order'=>'register DESC'),
			'comments' => array(self::HAS_MANY, 'Rate', 'movie_id', 'order'=>'register DESC', 'limit'=>5, 'scopes'=>array('commented')),
			'users' => array(self::MANY_MANY, 'User', 'user_movie(movie_id, user_id)'),
			'usersFavorite' => array(self::HAS_MANY, 'UserMovie', 'movie_id', 'limit'=>8, 'scopes'=>array('favorited')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'original_name' => 'Original Name',
			'slug' => 'Slug',
			'age' => 'Age',
			'time' => 'Time',
			'year' => 'Year',
			'review' => 'Review',
			'review_status' => 'Review Status',
			'release' => 'Release',
			'poster' => 'Poster',
			'register' => 'Register',
			'published_in' => 'Published In',
			'status' => 'Status',
		);
	}

	public function defaultScope()
	{
		$scope = array(
			'condition'=>'status=3',
			'limit'=>12
		);

		if(Yii::app()->request->getParam('page') AND Yii::app()->request->getParam('page') > 1) {
			$page = Yii::app()->request->getParam('page');
			$limit = Yii::app()->params['paginationLimit'];

			$scope['offset'] = ($page-1)*$limit;
		}

		return $scope;
	}

	public function scopes()
	{
		return array(
			'releases'=>array('condition'=>'date_format(`release`, \'%Y%m%d\') >= curdate() OR `release` is NULL'),
			// 'bestEvaluated'=>array('condition'=>'rating > 0', 'order'=>'rating DESC'),
			'mostViewed'=>array('condition'=>'evaluated > 0', 'order'=>'evaluated DESC'),
			'inTheaters'=>array('condition'=>'date_format(`release`, \'%Y%m%d\') <= curdate()', 'order'=>'`release` DESC'),
		);
	}

	public function afterFind()
	{
		$items = 0;

		if($this->favorited>0)
			$items++;
		if($this->commented>0)
			$items++;
		if($this->anticipated>0)
			$items++;

		if($items==0)
			$this->boxLenght = 'small';
		else if($items==1)
			$this->boxLenght = 'medium';
		else if($items>1)
			$this->boxLenght = '';
	}

	public function bestEvaluated()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'rating > :rating';
		$criteria->params = array(':rating'=>0);
		$criteria->order = 'rating DESC';

		$movies = self::model()->findAll($criteria);

		foreach($movies as $movie) {
			$criteria = new CDbCriteria;
			$criteria->condition = '`movie_id` = :movie_id AND `like` > :like';
			$criteria->params = array(':movie_id'=>$movie->id, ':like'=>0);
			$criteria->order = '`like` DESC';

			$comment = Rate::model()->find($criteria);

			if ($comment)
				$movie->comment = (object)array('text'=>$comment->comment, 'author'=>$comment->user->name, 'date'=>$comment->register);
		}

		return $movies;
	}

	public function search()
	{
		$movies = array();
		$term = Yii::app()->request->getParam('term');
		$ajax = (Yii::app()->request->getParam('ajax')=='true');

		if ($ajax)
			$limit = 2;
		else
			$limit = 4;

		$criteria = new CDbCriteria;
		$criteria->select = 'id, name, year, poster, slug';
		$criteria->condition = 'name LIKE :name OR original_name LIKE :original_name';
		$criteria->params = array(':name'=>"%{$term}%", ':original_name'=>"%{$term}%");
		$criteria->order = 'name';
		$criteria->limit = $limit;

		$data = self::model()->findAll($criteria);

		if ($ajax) {
			foreach($data as $movie) {
				$movies[] = array('id'=>$movie->id, 'name'=>$movie->name, 'image'=>$movie->poster, 'slug'=>$movie->slug, 'additional'=>$movie->year);
			}

			return $movies;
		}
		else {
			return $data;
		}
	}

	public function one($slug)
	{
		$data = self::model()->find('slug=:slug', array(':slug'=>$slug));
		return $data;
	}
}