<?php
// Load AWS SDK (V2)
require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'aws.phar';

// Load namespaces
use Aws\Common\Enum\Region;
use Aws\Ses\SesClient;

class Aws
{
	public static function sendMail($to, $subject, $body, $sender='noreply', $cc = array(), $bcc = array())
	{
		$ses = SesClient::factory(array(
			'key'    => Yii::app()->params['aws']['key'],
			'secret' => Yii::app()->params['aws']['secret'],
			'region' => Region::US_EAST_1
		));

		// To Array
		$to = is_array($to) ? $to : array($to);
		$cc = is_array($cc) ? $cc : array($cc);
		$bcc = is_array($bcc) ? $bcc : array($bcc);

		try {

			$status = $ses->sendEmail(array(
				'Source' => 'Pipocket <' . $sender . '@pipocket.com>',
			    'Destination' => array(
			        'ToAddresses' => $to,
			        'CcAddresses' => $cc,
			        'BccAddresses' => $bcc,
			    ),
			    'Message' => array(
			        'Subject' => array(
			            'Data' => $subject,
			            'Charset' => 'utf-8',
			        ),
			        'Body' => array(
			            'Html' => array(
			                'Data' => $body,
			                'Charset' => 'utf-8',
			            ),
			        ),
			    ),
			    'ReplyToAddresses' => array($sender . '@pipocket.com'),
			    'ReturnPath' => $sender . '@pipocket.com',
			));

			return $status;

		} catch( Exception $e ) {
			return false;
		}
	}
}