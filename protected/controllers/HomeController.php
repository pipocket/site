<?php
class HomeController extends Controller
{
	public function actionIndex()
	{
		// Set current filter name
		$this->currentFilterName();

		$this->render('index', array(
			'movies'=>$this->actionMovies(),
			'currentFilter'=>Yii::app()->request->getParam('filter') ? Yii::app()->request->getParam('filter') : 'estreias',
		));
	}

	public function actionMovies($ajax=false)
	{
		if(!Yii::app()->request->getParam('filter') OR Yii::app()->request->getParam('filter')=='estreias')
			$movies = Movie::model()->releases()->findAll();
		else if(Yii::app()->request->getParam('filter')=='melhores-avaliados')
			$movies = Movie::model()->bestEvaluated();
		else if(Yii::app()->request->getParam('filter')=='mais-vistos')
			$movies = Movie::model()->mostViewed()->findAll();
		else if(Yii::app()->request->getParam('filter')=='em-cartaz')
			$movies = Movie::model()->inTheaters()->findAll();
		else if(Yii::app()->request->getParam('filter')=='todos')
			$movies = Movie::model()->findAll(array('order'=>'name'));

		$data = $this->renderPartial('//movie/item', array('items'=>$movies, 'currentFilter'=>Yii::app()->request->getParam('filter'), 'ajax'=>$ajax), true);

		if($ajax)
			echo $data;
		else
			return $data;
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	private function currentFilterName()
	{
		switch (Yii::app()->request->getParam('filter')) {
			case 'estreias':
				$this->currentFilterName = 'Estréias';
				break;

			case 'melhores-avaliados':
				$this->currentFilterName = 'Melhores Avaliados';
				break;

			case 'mais-vistos':
				$this->currentFilterName = 'Mais Vistos';
				break;

			case 'em-cartaz':
				$this->currentFilterName = 'Em Cartaz';
				break;

			case 'todos':
				$this->currentFilterName = 'Todos';
				break;

			default:
				$this->currentFilterName = 'Estréias';
				break;
		}
	}

	public function actionMail()
	{
		$this->renderPartial('//email/register', array('name'=>'Paulo Martins', 'activationCode'=>'978dsa'));
	}
}