<?php

class AboutController extends Controller
{
	public function actionIndex()
	{
		$this->render('about');
	}

	public function actionPrivacy()
	{
		$this->render('privacy');
	}

	public function actionTerms()
	{
		$this->render('terms');
	}
}