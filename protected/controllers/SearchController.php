<?php

class SearchController extends Controller
{
	public function actionIndex()
	{
		$movies = Movie::model()->search();
		$cast = Cast::model()->search();

		if (Yii::app()->request->getParam('ajax')=='true') {
			$data = array('movies'=>$movies, 'cast'=>$cast);
			$this->renderPartial('list', array('data'=>$data));
		}
		else {
			$currentFilter = 'todos';
			$movies = $data = $this->renderPartial('//movie/item', array('items'=>$movies, 'currentFilter'=>$currentFilter, 'ajax'=>false), true);
			$cast = $data = $this->renderPartial('//cast/item', array('items'=>$cast), true);

			$this->render('index', array('currentFilter'=>$currentFilter, 'movies'=>$movies, 'cast'=>$cast));
		}
	}
}