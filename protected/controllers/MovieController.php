<?php

class MovieController extends Controller
{
	public function actionIndex()
	{
		$slug = Yii::app()->request->getParam('slug');
		$movie = Movie::model()->one($slug);

		if (Yii::app()->request->isAjaxRequest) {
			$movie = $this->clearJsonResponse($movie);
			$this->jsonResponse($movie);
		}
		else {
			$this->render('index', array('movie'=>$movie));
		}
	}

	public function actionDiscussion()
	{
		$rate_id = Yii::app()->request->getParam('rate_id');

		if ($rate_id) {
			$discussion = Discussion::model()->byComment($rate_id);
			$this->jsonResponse($discussion);
		}
	}

	private function clearJsonResponse($movie)
	{
		$data = array();

		$data['name'] 					= $movie->name;
		$data['original_name'] 	= $movie->original_name;
		$data['slug'] 					= $movie->slug;
		$data['genre']	 				= $this->dataSeparated($movie->genres, ',');
		$data['directors']			= $this->castDataSeparated($movie->directors, ',', 'directors');
		$data['actors']		 			= $this->castDataSeparated($movie->actors, ',', 'actors');
		$data['age'] 						= $movie->age;
		$data['time'] 					= $movie->time;
		$data['year'] 					= $movie->year;
		$data['review'] 				= $movie->review;
		$data['release'] 				= $movie->release ? $this->dateFormat($movie->release) : '';
		$data['poster'] 				= $movie->poster;
		$data['rating'] 				= $movie->rating;
		$data['usersFavorite']  = $this->favoritesDataSeparated($movie->usersFavorite);
		$data['rates']  				= $this->ratesDataSeparated($movie->rates);
		$data['comments']  			= $this->commentsDataSeparated($movie->comments);
		$data['ratesCount']  		= Rate::model()->count('movie_id = :movie_id', array(':movie_id'=>$movie->id));
		$data['favoritesCount']  = UserMovie::model()->count('movie_id = :movie_id AND favorite = :favorite', array(':movie_id'=>$movie->id, ':favorite'=>1));

		return (object)$data;
	}

	private function dataSeparated($items, $separator)
	{
		$data = null;

		foreach ( $items as $item ) {
			$data .= "{$item->name}, ";
		}

		return substr( $data, 0 , -2);
	}

	private function castDataSeparated($items, $separator, $type)
	{
		$data = array();
		$type = $type == 'directors' ? 'diretor' : 'ator';

		foreach ( $items as $index => $item ) {
			$data[$index]['name'] = $item->cast->name;
			$data[$index]['url'] = $this->createAbsoluteUrl("/{$type}/{$item->cast->slug}");
		}

		return $data;
	}

	private function ratesDataSeparated($items)
	{
		$data = array();

		foreach ( $items as $index => $item ) {
			$data[$index]['user_name'] = $item->user->name;
			$data[$index]['user_avatar'] = $item->user->avatar;
		}

		return $data;
	}

	private function favoritesDataSeparated($items)
	{
		$data = array();

		foreach ( $items as $index => $item ) {
			$data[$index]['user_name'] = $item->user->name;
			$data[$index]['user_avatar'] = $item->user->avatar;
		}

		return $data;
	}

	private function commentsDataSeparated($items)
	{
		$data = array();

		foreach ( $items as $index => $item ) {
			$data[$index]['id'] = $item->id;
			$data[$index]['comment'] = $item->comment;
			$data[$index]['like'] = $item->like;
			$data[$index]['unlike'] = $item->unlike;
			$data[$index]['register'] = $this->dateFormat($item->register);
			$data[$index]['user_name'] = $item->user->name;
			$data[$index]['user_avatar'] = $item->user->avatar;
		}

		return $data;
	}
	
}