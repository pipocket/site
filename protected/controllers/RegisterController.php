<?php

class RegisterController extends Controller
{
	public function actionIndex()
	{
		$this->onlyGuest();
		$this->render('index');
	}

	public function actionGo()
	{
		// Get user instance
		$model = new User;
		$model->attributes = $_POST['User'];

		// Set flash data
		Yii::app()->user->setFlash('name', $model->name);
		Yii::app()->user->setFlash('email', $model->email);

		// Verify user
		$checkUser = User::model()->find('email=:email', array(':email'=>$model->email));

		if (Yii::app()->request->getParam('validate')) {
			$this->jsonResponse( ($checkUser ? false : true) );
			exit;
		}

		// Create user
		if (!$checkUser) {
			if ($model->validate() AND $model->save()) {
				// Activate url
				$activationCode = $this->generateUserCode($model->id, $model->email);

				// Send Mail
				$this->sendActivationMail($model->email, $model->name, $activationCode);

				// Set flash status
				Yii::app()->user->setFlash('register', 'success');
			}
			else {
				// Set flash status
				Yii::app()->user->setFlash('register', 'error');
			}
		}
		else {
			Yii::app()->user->setFlash('register', 'user-exists');
		}

		$this->redirect($this->createAbsoluteUrl('/'));
	}

	public function actionActivationMail()
	{
		// Get activation code
		$activationCode = Yii::app()->session['activationCode'];
		$codeData = $this->decodeUserCode($activationCode);

		// Find user
		$model = User::model()->findByPk($codeData->id);
		
		if ($model) {
			// Send Mail
			$this->sendActivationMail($model->email, $model->name, $activationCode);

			// Set flash status
			Yii::app()->user->setFlash('register', 'success');
			// Set flash data
			Yii::app()->user->setFlash('name', $model->name);
			Yii::app()->user->setFlash('email', $model->email);
		}

		$this->redirect($this->createAbsoluteUrl('/'));
	}

	public function actionActivate()
	{
		$activationCode = Yii::app()->request->getParam('uid');

		if ($activationCode) {
			$codeData = $this->decodeUserCode($activationCode);

			// Find user
			$model = User::model()->findByPk($codeData->id);

			if ($model AND Bcrypt::verify($model->email, $codeData->email)) {
				// Change user status
				$model->status = 1;
				$model->save();

				// Set flash status
				Yii::app()->user->setFlash('register', 'activated');
			}
		}

		$this->redirect($this->createAbsoluteUrl('/'));
	}

	private function sendActivationMail($email, $name, $activationCode)
	{
		return Aws::sendMail(
			$email, 
			'Confirmação de cadastro', 
			$this->renderPartial('//email/register', array('title'=>'Confirmação de cadastro', 'name'=>$name, 'activationCode'=>$activationCode), true),
			'welcome'
		);
	}
}