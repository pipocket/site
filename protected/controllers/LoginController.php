<?php

class LoginController extends Controller
{
	private $identity;
	private $errorCode;

	public function actionIndex()
	{
		// Only guest user
		$this->onlyGuest();

		if (isset($_POST['User'])) {
			$status = $this->check( $_POST['User']['email'], $_POST['User']['password'] );
			$message = null;

			if ($status) {
				$this->defineLoginSession();
			}
			else {
				if ($this->errorCode===3)
					$message = 'Conta inativa';
				elseif ($this->errorCode===4)
					$message = 'Conta bloqueada';
				elseif ($this->errorCode===1 OR $this->errorCode===2)
					$message = 'Combinação de Usuário/Senha inválida';
			}

			$this->jsonResponse(array('status'=>$status, 'message'=>$message));
		}
	}

	public function actionFacebook()
	{
		// Only guest user
		$this->onlyGuest();

		// Init Facebook API
		Yii::import('ext.facebook.*');

		$facebook = new Facebook(array(
			'appId'  => Yii::app()->params['social']['facebook']['appId'],
			'secret' => Yii::app()->params['social']['facebook']['secret']
		));

		// Verify user
		$user = $facebook->getUser();

		// User accepts
		if ($user) {
			// Set access token to extended version
			$facebook->setExtendedAccessToken();

			// Get user data from api
			$userData = $facebook->api('/me');

			// Photo
			$photo = json_decode(file_get_contents("https://graph.facebook.com/{$userData['id']}/picture?redirect=false&type=large"));
			$userData['photo'] = $photo->data->url;

			// Save/Update user
			$status = User::model()->checkAndSave($userData, 'facebook', $userData['id'], $facebook->getAccessToken());

			if ($status) {
				$status = $this->check($userData['id'], null, 'facebook');

				if ($status) {
					// Define login session
					$this->defineLoginSession();

					// Send to home
					$this->redirect( $this->createAbsoluteUrl('/') );
				}
			}
		}
		// Oauth
		else {
			$loginUrl = $facebook->getLoginUrl( array('scope'=>'email') );
			$this->redirect( $loginUrl );
		}

		/*
			$facebook->setAccessToken(string);
			$friends = $facebook->api('/me/friends');
		*/
	}

	public function actionTwitter()
	{
		// Only guest user
		$this->onlyGuest();

		// Init Twitter API
		Yii::import('ext.twitter.*');

		// Get params
		$denied = Yii::app()->request->getParam('denied');
		$oauthToken = Yii::app()->request->getParam('oauth_token');
		$oauthVerifier = Yii::app()->request->getParam('oauth_verifier');

		// User cancels
		if ($denied) {
			$this->redirect( $this->createAbsoluteUrl('/') );
			exit;
		}

		// User accepts
		if ($oauthToken AND $oauthVerifier) {
			$twitter = new TwitterOAuth(
				Yii::app()->params['social']['twitter']['consumerKey'],
				Yii::app()->params['social']['twitter']['consumerSecret'],
				Yii::app()->session['oauthToken'],
				Yii::app()->session['oauthTokenSecret']
			);

			// Get access token
			Yii::app()->session['accessToken'] = $twitter->getAccessToken( $oauthVerifier );

			// Kill sessions unused
			unset( Yii::app()->session['oauthToken'] );
			unset( Yii::app()->session['oauthTokenSecret'] );

			// Get user data from api
			$user = $twitter->get('account/verify_credentials');

			// Photo
			$user->profile_image_url = str_replace('_normal', '', $user->profile_image_url);

			// Save/Update user
			$status = User::model()->checkAndSave($user, 'twitter', $user->id, Yii::app()->session['accessToken']);

			if ($status) {
				$status = $this->check($user->id, null, 'twitter');

				if ($status) {
					// Define login session
					$this->defineLoginSession();

					// Send to home
					$this->redirect( $this->createAbsoluteUrl('/') );
				}
			}
		}
		else {
			$twitter = new TwitterOAuth(
				Yii::app()->params['social']['twitter']['consumerKey'],
				Yii::app()->params['social']['twitter']['consumerSecret']
			);

			$requestToken = $twitter->getRequestToken( $this->createAbsoluteUrl( Yii::app()->params['social']['twitter']['oauthCallback'] ) );

			Yii::app()->session['oauthToken'] = $requestToken['oauth_token'];
			Yii::app()->session['oauthTokenSecret'] = $requestToken['oauth_token_secret'];

			if ($twitter->http_code == 200)
				$this->redirect( $twitter->getAuthorizeURL( Yii::app()->session['oauthToken'] ) );
		}

		/*
			$friends = $twitter->get('followers/list');
		*/
	}

	public function actionGooglePlus()
	{
		// Only guest user
		$this->onlyGuest();

		// Init Facebook API
		Yii::import('ext.googleplus.*');

		$client = new Google_Client();
		$plus = new Google_PlusService($client);
		$client->setApplicationName( Yii::app()->params['social']['google']['applicationName'] );
		$client->setClientId( Yii::app()->params['social']['google']['clientId'] );
		$client->setClientSecret( Yii::app()->params['social']['google']['clientSecret'] );
		$client->setRedirectUri( $this->createAbsoluteUrl( Yii::app()->params['social']['google']['redirectUri'] ) );

		// Get params
		$code = Yii::app()->request->getParam('code');
		$error = Yii::app()->request->getParam('error');
		
		// User cancels
		if ($error) {
			$this->redirect( $this->createAbsoluteUrl('/') );
			exit;
		}

		// User accepts
		if ($code) {
			$client->authenticate($_GET['code']);

			// Get access token
			Yii::app()->session['accessToken'] = $client->getAccessToken();

			// Get user data from api
			$user = $plus->people->get('me');

			// Photo
			$user['image']['url'] = str_replace('?sz=50', '', $user['image']['url']);

			// Save/Update user
			$status = User::model()->checkAndSave($user, 'googleplus', $user['id'], Yii::app()->session['accessToken']);

			if ($status) {
				$status = $this->check($user['id'], null, 'googleplus');

				if ($status) {
					// Define login session
					$this->defineLoginSession();

					// Send to home
					$this->redirect( $this->createAbsoluteUrl('/') );
				}
			}
		}
		else {
			$loginUrl = $client->createAuthUrl();
			$this->redirect( $loginUrl );
		}
	}

	public function actionForgotPassword()
	{
		// Only guest user
		$this->onlyGuest();

		if (isset($_POST['User']['email'])) {
			if (!$_POST['User']['email']) {
				Yii::app()->user->setFlash('forgot-password', 'empty');
			}
			else if (!filter_var($_POST['User']['email'], FILTER_VALIDATE_EMAIL)) {
				Yii::app()->user->setFlash('forgot-password', 'invalid');
			}
			else {
				Yii::app()->user->setFlash('email', $_POST['User']['email']);
				$model = User::model()->find('email=:email', array(':email'=>$_POST['User']['email']));

				if ($model) {
					// Activate url
					$activationCode = $this->generateUserCode($model->id, $model->email);

					// Inactive user
					if ($model->status==0) {
						Yii::app()->session['activationCode'] = $activationCode;
						Yii::app()->user->setFlash('forgot-password', 'inactive');
					}
					else {
						// Send Mail
						Aws::sendMail(
							$model->email, 
							'Recuperação de senha', 
							$this->renderPartial('//email/forgot-password', array('title'=>'Recuperação de senha', 'name'=>$model->name, 'activationCode'=>$activationCode), true)
						);

						// Set status
						Yii::app()->user->setFlash('forgot-password', 'success');
					}
				}
				else {
					Yii::app()->user->setFlash('forgot-password', 'error');
				}
			}
		}

		$this->render('forgot-password');
	}

	public function actionResetPassword()
	{
		// Only guest user
		$this->onlyGuest();

		// Get reset param
		$uid = Yii::app()->request->getParam('uid');
		$emailSession = Yii::app()->session['reset_password_email'];

		// If form submited and session exists
		if (isset($_POST['User']['password']) AND isset($_POST['User']['passwordConfirm']) AND $emailSession) {
			if (!$_POST['User']['password'] OR !$_POST['User']['passwordConfirm']) {
				Yii::app()->user->setFlash('reset-password', 'empty');
			}
			else {
				$model = User::model()->find('email=:email', array(':email'=>$emailSession));
				$model->scenario = 'formResetPassword';
				$model->attributes = $_POST['User'];
				
				if ($model->validate() AND $model->save()) {
					Yii::app()->user->setFlash('reset-password', 'success');
					Yii::app()->user->setFlash('hide-form', true);
				}
				else {
					if (isset($model->errors['passwordConfirm'][0])) {
						Yii::app()->user->setFlash('reset-password', 'error-password');
					}
					else if ($model->errors['password'][0]) {
						Yii::app()->user->setFlash('reset-password', 'error-password-short');
					}
				}
			}

			// Get UID
			$uid = Yii::app()->session['reset_password_uid'];
			// Kill UID Session
			unset(Yii::app()->session['reset_password_uid']);
			// Kill email Session
			unset(Yii::app()->session['reset_password_email']);

			$this->redirect( $this->createUrl('/login/redefinir-senha') . '/?uid=' . $uid );
			exit;
		}
		// If reset param exists
		else if ($uid) {
			// UID
			Yii::app()->session['reset_password_uid'] = $uid;

			// Split data
			$split = explode(Yii::app()->params['securityResetPasswordHash'], $uid);
			// Get user id
			$id = base64_decode(reset($split));
			// Get user email
			$email = array_pop($split);

			// Find user
			$model = User::model()->findByPk($id);

			if ($model AND Bcrypt::verify($model->email, $email)) {
				Yii::app()->session['reset_password_email'] = $model->email;
			}
		}
		// Else, send to home
		else {
			$this->redirect( $this->createAbsoluteUrl('/') );
			exit;
		}

		$this->render('reset-password');
	}

	public function actionExit()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	private function check($username, $password, $socialLogin=null)
	{
		$this->identity = new UserIdentity($username, $password, $socialLogin);
		$this->errorCode = $this->identity->authenticate();

		return $this->errorCode === 0 ? true : false;
	}

	private function defineLoginSession()
	{
		Yii::app()->user->login($this->identity, Yii::app()->params['loginDuration']);
		Yii::app()->session['logged'] = date('Y-m-d H:i:s');
	}
}