<?php

class UserController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function ActionEmail()
	{
		// User data
		$name = 'Paulo Martins';
		$email = 'phmartins@gmail.com';
		$encryptedEmail = base64_encode($email);

		// Mail content
		$body = $this->renderPartial('//email/register', array('name'=>$name, 'email'=>$encryptedEmail), true);

		// Send mail class
		$simpleMail = new SimpleMail;
		$simpleMail->subject = 'Boas vindas!';
		$simpleMail->from_name = 'Pipocket';
		$simpleMail->from_email = 'noreply@pipocket.com';
		$simpleMail->to_name = $name;
		$simpleMail->to_email = $email;
		$simpleMail->body = $body;
		var_dump($simpleMail->basic());
	}
}