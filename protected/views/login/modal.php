<?php if (Yii::app()->user->isGuest): ?>
<!-- Login Modal -->
<div class="md-modal md-effect-7" id="login-register">
    <div class="md-content">
        <div class="shadow-bar left"></div>
        <button class="md-close icons modalClose ir" title="Fechar janela"><!-- Close me! --></button>
        <div class="md-main login">
            <h3>ENTRAR NO PIPOCKET</h3>

            <div class="clearfix">
                <div class="left">
                    <!-- Conecte-se com o Facebook -->
                    <div>
                        <a href="<?php echo $this->createUrl('/login/facebook') ?>" class="buttons social-facebook ir" rel="socialConnect">Facebook login</a>
                    </div>

                    <span>ou</span>

                    <!-- Conecte-se com o Twitter -->
                    <div>
                        <a href="<?php echo $this->createUrl('/login/twitter') ?>" class="buttons social-twitter ir" rel="socialConnect">Twitter login</a>
                    </div>

                    <span>ou</span>

                    <!-- Conecte-se com o Google+ -->
                    <div>
                        <a href="<?php echo $this->createUrl('/login/googleplus') ?>" class="buttons social-google ir" rel="socialConnect">Google+ login</a>
                    </div>
                </div>
                <div class="right">
                    <form action="<?php echo $this->createUrl('/login') ?>" method="POST" name="login" data-message="Preencha todos os campos">
                        <fieldset>
                            <label>E-mail:</label>
                            <input type="text" name="User[email]" class="default-field email" data-message="Digite um e-mail válido" autocomplete="off">
                        </fieldset>
                        <fieldset>
                            <label>Senha:</label>
                            <div class="box-password clearfix">
                                <input type="password" name="User[password]" class="default-field password">
                                <input type="submit" class="go default-button" value="ENTRAR">
                            </div>
                            <p><a href="<?php echo $this->createUrl('/login/esqueci-minha-senha') ?>" class="default-link">Esqueci minha senha</a></p>
                            <p>Novo no Pipocket? <a href="<?php echo $this->createUrl('/cadastro') ?>" class="default-link">Cadastre-se agora</a></p>
                        </fieldset>
                        <div class="error hide"><p></p></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="shadow-bar right"></div>
    </div>
</div>
<div class="md-overlay"></div>
<?php endif ?>