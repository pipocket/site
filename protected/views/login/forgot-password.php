<div class="main-area">
	<h2>Esqueci minha senha</h2>

	<div class="main forgot-password">
		<p>Informe o e-mail que utilizou ao realizar seu cadastro, lhe enviaremos as instruções para redefinição de senha:</p>

		<?php if (Yii::app()->user->hasFlash('forgot-password')): ?>
            <?php $this->renderPartial('//common/message', array('type'=>'forgot-password', 'status'=>Yii::app()->user->getFlash('forgot-password'))) ?>
        <?php endif ?>

		<form action="<?php echo $this->createUrl('/login/esqueci-minha-senha') ?>" method="POST" class="clearfix">
			<fieldset>
				<label>E-mail:</label>
				<input type="text" name="User[email]" class="default-field" />
			</fieldset>
			<fieldset class="btn">
				<input type="submit" class="default-button" value="RECUPERAR MINHA SENHA">
			</fieldset>
		</form>
	</div>
</div>