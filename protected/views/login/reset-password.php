<div class="main-area">
	<h2>Redefinir senha</h2>

	<div class="main reset-password">
		<?php if (!Yii::app()->user->hasFlash('hide-form')): ?>
			<p>Cadastre uma nova senha para acessar sua conta no Pipocket</p>
		<?php endif ?>

		<?php if (Yii::app()->user->hasFlash('reset-password')): ?>
            <?php $this->renderPartial('//common/message', array('type'=>'reset-password', 'status'=>Yii::app()->user->getFlash('reset-password'))) ?>
        <?php endif ?>

		<?php if (!Yii::app()->user->hasFlash('hide-form')): ?>
			<form action="<?php echo $this->createUrl('/login/redefinir-senha') ?>" method="POST" class="clearfix">
				<fieldset>
					<label>Senha:</label>
					<input type="password" name="User[password]" class="default-field" />
				</fieldset>
				<fieldset>
					<label>Confirme sua senha:</label>
					<input type="password" name="User[passwordConfirm]" class="default-field" />
				</fieldset>
				<fieldset class="btn">
					<input type="submit" class="default-button" value="RECADASTRAR SENHA">
				</fieldset>
				<p>Crie uma senha com 6 caracteres ou mais</p>
			</form>
		<?php endif ?>
	</div>
</div>