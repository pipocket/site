<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="center">
			<table width="600" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td bgcolor="#FFFFFF" style="padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px;">
						<p style="font-size:12px;color:#646363;line-height:17px;margin:0;font-family:Georgia,'Arial'">Esqueceu sua senha? <a href="#" style="text-decoration:none;color:#00a2cc;">Veja as instruções</a> para recupera-la.</p>
						<p style="font-size:12px;color:#646363;line-height:17px;margin:0;font-family:Georgia,'Arial'">Caso não queira receber notificações, altere as <a href="#" style="text-decoration:none;color:#00a2cc;">configuraçoes de notificação</a> de sua conta.</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>