<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Pipocket <?php echo isset($title) ? '- ' . $title : '' ?></title>
</head>
<body bgcolor="#f7f7f7">
	<div style="width:100%;background-color:#f7f7f7;background-image:url(<?php echo Yii::app()->params['aws']['s3BaseUrl'] . 'email/bg.jpg' ?>)">
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td align="center">
				<table width="600" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="center" style="border-bottom:1px solid #f0f0f0;padding-top:20px;padding-bottom:20px;">
							<img src="<?php echo Yii::app()->params['aws']['s3BaseUrl'] . 'email/logo.gif' ?>" alt="Pipocket" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>