<?php echo $this->renderPartial('//email/header') ?>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="center">
			<table width="600" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td style="padding-top:20px;padding-bottom:100px;padding-left:20px;padding-right:20px;">
						<p style="font-size:14px;color:#4d4a4a;line-height:25px;margin:0;font-family:Georgia,'Arial'">Olá <strong><?php echo $name ?></strong>, você solicitou a recuperação de senha, para prosseguir <a href="<?php echo $this->createAbsoluteUrl('/login/redefinir-senha') . '/?uid=' . $activationCode ?>" style="text-decoration:none;color:#00a2cc;">clique neste link</a> e siga as instruções.</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $this->renderPartial('//email/footer') ?>