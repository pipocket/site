<div class="main-area">
	<div class="clearfix">
        <div class="left">
            <h4>FILME XPTO</h4>
            <h5>(Iron Man)</h5>

            <div class="poster">
                <img src="https://s3-sa-east-1.amazonaws.com/pipocket/movie-1321.jpg" alt="Nome do filme">
            </div>
        </div>
        <div class="right">
            <div class="movie-header">
                <ul class="unstyled clearfix">
                    <li class="release"><span class="icons calendar"></span> 26/04/2013</li>
                    <li class="age"><span class="icons age"></span> 10 anos</li>
                    <li class="time"><span class="icons time"></span> 157 min.</li>
                </ul>
            </div>
            <div class="movie-info">
                <label>Gênero</label>
                <p>Aventura</p>

				<div class="tablet-desktop-info">
	                <label>Direção</label>
	                <p>Shane Black</p>

	                <label>Atores principais</label>
	                <p><a href="#">Robert Downey Jr.</a>, Gwyneth Paltrow, <a href="#">Guy Pearce</a>, Ben Kingsley, Don Cheadle, Ben Kingsley, Jessica Chastain, Drew Pearce.</p>

	                <label>Sinopse</label>
	                <p>Homem de Ferro 3 traz o industrial, arrogante, 
	                porém brilhante, Tony Stark/Homem de Ferro contra um inimigo cujo alcance não tem limites. Quando Stark vê seu mundo pessoal destruído pelas mãos 
	                de seu inimigo, ele embarca em uma angustiante jornada para encontrar os responsáveis. Uma jornada que a cada reviravolta seus brios serão testados. Pressionado, Stark terá que sobreviver lançando 
	                mão de seus próprios dispositivos, contando com 
	                sua engenhosidade e instintos para proteger aqueles 
	                que lhe são mais próximos. Em sua luta para retornar, Stark descobre a resposta para a pergunta que 
	                o atormenta em segredo: o homem faz o traje 
				</div>
                ou é o traje que faz o homem?</p>
            </div>
            <div class="share">
                <label>Compartilhar:</label>
                <a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a>
                <div class="g-plusone" data-size="medium"></div>

                <div class="like clearfix">
                    <a href="#" class="buttons like" alt="Curtir o filme" title="Curtir o filme">CURTI O FILME</a>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-info">
		<label>Direção</label>
		<p>Shane Black</p>

		<label>Atores principais</label>
		<p><a href="#">Robert Downey Jr.</a>, Gwyneth Paltrow, <a href="#">Guy Pearce</a>, Ben Kingsley, Don Cheadle, Ben Kingsley, Jessica Chastain, Drew Pearce.</p>

    	<label>Sinopse</label>
        <p>Homem de Ferro 3 traz o industrial, arrogante, 
        porém brilhante, Tony Stark/Homem de Ferro contra um inimigo cujo alcance não tem limites. Quando Stark vê seu mundo pessoal destruído pelas mãos 
        de seu inimigo, ele embarca em uma angustiante jornada para encontrar os responsáveis. Uma jornada que a cada reviravolta seus brios serão testados. Pressionado, Stark terá que sobreviver lançando 
        mão de seus próprios dispositivos, contando com 
        sua engenhosidade e instintos para proteger aqueles 
        que lhe são mais próximos. Em sua luta para retornar, Stark descobre a resposta para a pergunta que 
        o atormenta em segredo: o homem faz o traje 
        ou é o traje que faz o homem?</p>
    </div>
</div>
