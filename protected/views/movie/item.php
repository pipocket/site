<?php foreach ($items as $i=>$movie): ?>
<div class="box">
	<div class="movie <?php echo $movie->boxLenght ?>">
		<?php if ($currentFilter AND $currentFilter!='todos' AND $currentFilter!='estreias' AND $currentFilter!='em-cartaz'): ?>
		<?php
			$class = '';

			if($currentFilter=='melhores-avaliados')
				$class = 'valued';
			elseif($currentFilter=='em-cartaz' OR $currentFilter=='todos')
				$class = 'valued-min';
		?>
		<span class="marker <?php echo $class ?> ">
			<?php if($currentFilter=='melhores-avaliados'): ?>
			<span class="left text">Nota</span>
			<span class="right number"><?php echo $movie->rating ?></span>
			<?php elseif($currentFilter=='mais-vistos'): ?>
			<span class="left"><?php echo $movie->evaluated ?></span>
			<span class="right"><?php echo $this->terms('attended', $movie->evaluated); ?></span>
			<?php else: ?>
			<span class="left"><?php echo $movie->rating ?></span>
			<?php endif; ?>
		</span>	
		<?php endif ?>

		<div class="area">
			<div class="poster">
				<a href="#" class="hover watch icons ir">Watch</a>
				<a href="#" class="hover favorite icons ir">Favorite</a>

				<a href="<?php echo $this->createUrl('/filme/'.$movie->slug) ?>" class="overlay md-trigger" data-modal="md-movie">
					<img src="<?php echo Yii::app()->params['aws']['s3BaseUrl'] . $this->getImage($movie->poster, 'medium') ?>" alt="">
				</a>
			</div>
			<p class="ttl"><?php echo $movie->name ?></p>
			<p class="desc">
				<?php if (!$currentFilter OR $currentFilter=='estreias'): ?>
				<?php echo $this->daysLeft($movie->release) ?>
				<?php else: ?>
				<?php if ($movie->evaluated AND ($currentFilter=='melhores-avaliados' OR $currentFilter=='todos') ): ?>
				<?php echo $this->terms('evaluatedBy', $movie->evaluated); ?>
				<?php endif ?>
				<?php endif ?>
			</p>
		</div>

		<?php if ($movie->favorited OR $movie->commented OR $movie->anticipated): ?>
		<div class="info">
			<?php if ($movie->favorited): ?>
			<div class="line"><span class="icons favorite"></span><?php echo $this->terms('favorite', $movie->favorited); ?></div>
			<?php endif ?>
			<?php if ($movie->commented): ?>
			<div class="line"><span class="icons icon-comment"></span><?php echo $this->terms('comment', $movie->commented); ?></div>
			<?php endif ?>
			<?php if ($currentFilter=='estreias' AND $movie->anticipated): ?>
			<div class="line"><span class="icons want-to-watch"></span> <?php echo $this->terms('wantToWatch', $movie->anticipated); ?></div>
			<?php endif ?>
		</div>
		<?php endif ?>
	</div>

	<?php if ($movie->comment): ?>
	<div class="comment">
		<div class="user-data clearfix">
			<span class="photo">
				<img src="<?php echo $this->baseUrl ?>/public/images/tmp-user.jpg" alt="">
			</span>
			<span class="info">
				<p class="name"><?php echo $movie->comment->author ?></p>
				<p class="date"><?php echo $this->whenOccurred($movie->comment->date); ?></p>
			</span>
		</div>
		<div class="user-comment">
			<p><?php echo $movie->comment->text ?></p>
		</div>
	</div>	
	<?php endif ?>

	<!-- <div class="alert-blue">
		<div class="title clearfix">
			<span class="icon social-network"></span>
			<span class="subtitle">Facebook e Twitter</span>
		</div>
		<div class="info">
			<p>Link suas contas do Facebook e Twitter para ver as atividades dos seus amigos no Pipocket!</p>
		</div>
	</div> -->
</div>
<?php endforeach ?>
<?php if (!$items AND $ajax): ?>false<?php endif ?>