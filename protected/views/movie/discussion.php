<!-- Discussion Modal -->
<div class="md-wrap wrap-discussion">
    <div class="md-modal md-effect-7" id="md-discussion">
        <div class="md-content">
            <div class="shadow-bar left"></div>
            <button class="md-close icons modalClose ir" title="Fechar janela"><!-- Close me! --></button>
            <div class="md-main">
                <div class="clearfix">
                    <div class="left left-header">
                        <h4 class="name"></h4>
                        <h5 class="original_name"></h5>
                    </div>

                    <div class="right right-header">
                        <div class="movie-header">
                            <ul class="unstyled clearfix">
                                <li class="release"><span class="icons calendar"></span> <span class="text"></span></li>
                                <li class="age"><span class="icons age"></span> <span class="text"></span></li>
                                <li class="time"><span class="icons time"></span> <span class="text"></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix container-movie-info">
                    <div class="left left-discussion">
                        <div class="poster">
                            <img src="" data-base-url="<?php echo Yii::app()->params['aws']['s3BaseUrl'] ?>" alt="">
                        </div>
                    </div>

                    <div class="right right-discussion">
                        <div class="movie-info">
                            <label>Sinopse</label>
                            <p class="review"></p>
                        </div>

                        <div class="line"></div>

                        <div class="comment clearfix">
                            <div class="photo">
                                <img src="<?php echo $this->createAbsoluteUrl('/public/images/tmp-user-header.jpg') ?>" alt="Author Name">
                            </div>
                            <div class="text">
                                <p class="author"><span class="author-name">Username</span> em <span class="author-date">15/01/2014</span>:</p>
                                <p class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                <div class="actions clearfix">
                                    <a href="#" class="buttons like-comment up">114</a>
                                    <a href="#" class="buttons unlike-comment down">34</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="line"></div>

                <div class="comments">
                    <label class="lbl-discussion">Discussão sobre o comentário de <span class="comment_author">Fulano da silva</span>:</label>

                    <div class="comment-model hidden">
                        <div class="comment clearfix">
                            <div class="photo">
                                <img src="<?php echo $this->createAbsoluteUrl('/public/images/tmp-user-header.jpg') ?>" alt="Author Name">
                            </div>
                            <div class="text">
                                <p class="author"><span class="author-name">Username</span> em <span class="author-date">15/01/2014</span>:</p>
                                <p class="message"></p>
                            </div>
                            <div class="actions">
                                <div class="clearfix">
                                    <a href="#" class="buttons like-comment up">114</a>
                                    <a href="#" class="buttons unlike-comment down">34</a>
                                </div>
                            </div>
                        </div>
                        <div class="line"></div>
                    </div>
                    <div class="real-comments">
                    </div>
                    <div class="comment send-comment">
                        <div class="clearfix">
                            <div class="photo">
                                <img src="<?php echo $this->createAbsoluteUrl('/public/images/tmp-user-header.jpg') ?>" alt="Author Name">
                            </div>
                            <div class="text">
                                <p class="author">Escreva seu comentário:</p>
                                <textarea name="" rows="4" class="default-field"></textarea>
                                <span class="info">300 caracteres restantes</span>
                            </div>
                        </div>
                        <div class="button clearfix">
                            <input type="submit" class="default-button" value="COMENTAR">
                        </div>
                    </div>
                </div>
            </div>
            <div class="shadow-bar right"></div>
        </div>
    </div>
    <div class="md-overlay"></div>
</div>