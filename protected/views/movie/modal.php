<!-- Movie Modal -->
<div class="md-wrap wrap-movie">
    <div class="md-modal md-effect-7" id="md-movie">
        <div class="md-content">
            <div class="shadow-bar left"></div>
            <button class="md-close icons modalClose ir" title="Fechar janela"><!-- Close me! --></button>
            <div class="md-main">
                <div class="clearfix">
                    <div class="left left-header">
                        <h4 class="name"></h4>
                        <h5 class="original_name"></h5>
                    </div>

                    <div class="right right-header">
                        <div class="movie-header">
                            <ul class="unstyled clearfix">
                                <li class="release"><span class="icons calendar"></span> <span class="text"></span></li>
                                <li class="age"><span class="icons age"></span> <span class="text"></span></li>
                                <li class="time"><span class="icons time"></span> <span class="text"></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix container-movie-info">
                    <div class="left">
                        <div class="poster">
                            <img src="" data-base-url="<?php echo Yii::app()->params['aws']['s3BaseUrl'] ?>" alt="">
                        </div>
                    </div>

                    <div class="right">
                        <div class="movie-info">
                            <label>Gênero</label>
                            <p class="genre"></p>

                            <label>Direção</label>
                            <p class="directors"></p>

                            <label>Atores principais</label>
                            <p class="actors"></p>

                            <label>Sinopse</label>
                            <p class="review"></p>
                        </div>

                        <div class="line"></div>

                        <div class="share">
                            <div class="clearfix">
                                <label>Compartilhar:</label>
                                
                                <div class="share-buttons">
                                    <a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a>
                                    <div class="g-plusone" data-size="medium"></div>
                                </div>
                            </div>

                            <div class="like clearfix">
                                <a href="#" class="buttons like" alt="Curtir o filme" title="Curtir o filme">CURTIR O FILME</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vote">
                    <div class="clearfix">
                        <div class="rating">
                            <label>Média</label>
                            <span class="text"></span>
                        </div>

                        <div class="rated-by">
                            <label>Avaliaram este filme</label>
                            <div class="photos">
                                <img src="<?php echo $this->createAbsoluteUrl('/public/images/tmp-user-header.jpg') ?>" alt="User photo" title="User name" width="52" height="52">
                                <span class="rates-count">+ 47 pessoas</span>
                            </div>
                        </div>
                    </div>
                    <div class="your-rating clearfix">
                        <label>SUA NOTA</label>

                        <ul class="unstyled clearfix">
                            <li class="icons">1</li>
                            <li class="icons">2</li>
                            <li class="icons">3</li>
                            <li class="icons">4</li>
                            <li class="icons">5</li>
                            <li class="icons">6</li>
                            <li class="icons active">7</li>
                            <li class="icons">8</li>
                            <li class="icons">9</li>
                            <li class="icons">10</li>
                        </ul>
                    </div>
                </div>
                <div class="favorite clearfix">
                    <div class="favorited-by">
                        <label>Favoritaram este filme</label>
                        <div class="photos">
                            <img src="<?php echo $this->createAbsoluteUrl('/public/images/tmp-user-header.jpg') ?>" alt="User photo" title="User name" width="52" height="52">
                            <span class="favorites-count">+ 47 pessoas</span>
                        </div>
                    </div>

                    <a href="#" class="buttons fav">FAVORITAR FILME</a>
                </div>
                
                <div class="line"></div>

                <div class="comments">
                    <label data-comments-text="Comentários sobre o filme" data-discussion-text="Discussão sobre o comentário:">Comentários sobre o filme</label>

                    <div class="box-discussion-comment">
                        <div class="comment clearfix comment-discussion">
                            <div class="photo">
                                <img src="<?php echo $this->createAbsoluteUrl('/public/images/tmp-user-header.jpg') ?>" alt="Author Name">
                            </div>
                            <div class="text">
                                <p class="author"><span class="author-name">Username</span> em <span class="author-date">15/01/2014</span>:</p>
                                <p class="message">Bla bla</p>
                            </div>
                            <div class="actions">
                                <div class="clearfix">
                                    <a href="#" class="buttons like-comment up">114</a>
                                    <a href="#" class="buttons unlike-comment down">34</a>
                                </div>
                            </div>
                        </div>
                        <div class="line"></div>
                    </div>

                    <div class="comment-model hidden">
                        <div class="comment clearfix">
                            <div class="photo">
                                <img src="<?php echo $this->createAbsoluteUrl('/public/images/tmp-user-header.jpg') ?>" alt="Author Name">
                            </div>
                            <div class="text">
                                <p class="author"><span class="author-name">Username</span> em <span class="author-date">15/01/2014</span>:</p>
                                <p class="message"></p>
                            </div>
                            <div class="actions">
                                <div class="clearfix">
                                    <a href="#" class="buttons like-comment up">114</a>
                                    <a href="#" class="buttons unlike-comment down">34</a>
                                </div>
                                <input type="hidden" name="rate_id" value="">
                                <a href="#" class="reply" rel="reply-comment">Responder comentário</a>
                            </div>
                        </div>
                        <div class="line"></div>
                    </div>

                    <div class="real-comments"></div>

                    <div class="real-discussion"></div>

                    <div class="comment send-comment">
                        <div class="clearfix">
                            <div class="photo">
                                <img src="<?php echo $this->createAbsoluteUrl('/public/images/tmp-user-header.jpg') ?>" alt="Author Name">
                            </div>
                            <div class="text">
                                <p class="author">Escreva seu comentário:</p>
                                <textarea name="" rows="4" class="default-field"></textarea>
                                <span class="info">300 caracteres restantes</span>
                            </div>
                        </div>
                        <div class="button clearfix">
                            <input type="submit" class="default-button" value="COMENTAR">
                        </div>
                    </div>
                </div>
            </div>
            <div class="shadow-bar right"></div>
        </div>
    </div>
    <div class="md-overlay"></div>
</div>