<?php 
	$messageType = $status;

	switch ($status) {
		case 'user-exists':
			$messageType = 'warning';
			break;
		case 'empty':
		case 'inactive':
			$messageType = 'warning';
			break;
		case 'error-password':
		case 'error-password-short':
		case 'invalid':
			$messageType = 'error';
			break;
		case 'activated':
			$messageType = 'success';
			break;
	}
?>

<div class="alert-message alert-<?php echo $type ?> alert-<?php echo $messageType ?>">
	<?php if ($type=='register'): ?>
		<?php if ($status=='success'): ?>
		<span class="icons check"></span>
    	<p>Seja bem-vindo <strong><?php echo Yii::app()->user->getFlash('name') ?></strong>, em alguns instantes você receberá um e-mail em <strong><?php echo Yii::app()->user->getFlash('email') ?></strong>, utilize-o para ativar sua conta</p>
		<?php elseif($status=='error'): ?>
		<span class="icons notify"></span>
		<p>Ocorreu um erro ao tentar gravar os dados, por favor, tente novamente em alguns instantes</p>
		<?php elseif($status=='user-exists'): ?>
		<span class="icons notify"></span>
		<p>Já existe um usuário com o e-mail <strong><?php echo Yii::app()->user->getFlash('email') ?></strong>, efetue o login ou utilize a recuperação de senha</p>
		<?php elseif($status=='activated'): ?>
		<span class="icons check"></span>
		<p>Obrigado por se cadastrar no Pipocket, faça o <a href="#" class="md-trigger" data-modal="login-register">login</a> e divirta-se!</p>
		<?php endif ?>

	<?php elseif($type=='forgot-password'): ?>
		<?php if ($status=='success'): ?>
		<span class="icons check"></span>
    	<p class="tal">Muito bem! Em alguns instantes você receberá um e-mail em <strong><?php echo Yii::app()->user->getFlash('email') ?></strong>, utilize-o para redefinir sua senha.</p>
		<?php elseif($status=='error'): ?>
		<span class="icons notify"></span>
		<p class="tal">O e-mail <strong><?php echo Yii::app()->user->getFlash('email') ?></strong> não foi encontrado</p>
		<?php elseif($status=='inactive'): ?>
		<span class="icons notify"></span>
		<p class="tal">A conta relacionada ao e-mail <strong><?php echo Yii::app()->user->getFlash('email') ?></strong> não esta ativa. <a href="<?php echo $this->createUrl('/cadastro/email-ativacao') ?>">Clique aqui</a> para receber um novo e-mail de ativação</p>
		<?php elseif($status=='empty'): ?>
		<span class="icons notify"></span>
		<p class="tal">Você precisa digitar um e-mail!</p>
		<?php elseif($status=='invalid'): ?>
		<span class="icons notify"></span>
		<p class="tal">Digite um e-mail válido!</p>
		<?php endif ?>

	<?php elseif($type=='reset-password'): ?>
		<?php if ($status=='success'): ?>
		<span class="icons check"></span>
    	<p class="tal">Muito bem! Sua senha foi redefinida com sucesso e você já pode fazer o <a href="#" class="md-trigger" data-modal="login-register">login</a></p>
		<?php elseif($status=='error-password'): ?>
		<span class="icons notify"></span>
		<p class="tal">As senhas digitadas não conferem</p>
		<?php elseif($status=='error-password-short'): ?>
		<span class="icons notify"></span>
		<p class="tal">Sua senha deve possuir no mínimo <strong>6 caracteres</strong> entre letras e números</p>
		<?php elseif($status=='empty'): ?>
		<span class="icons notify"></span>
		<p class="tal">Você precisa digitar sua nova senha!</p>
		<?php endif ?>
	<?php endif ?>
</div>