<?php
	$this->cs->registerScript('register', 'register();');
?>

<div class="main-area">
	<h2>Cadastre-se</h2>

	<div class="main register clearfix">
		<div class="social">
			<a href="<?php echo $this->createUrl('/login/facebook') ?>" class="buttons social-facebook ir">Facebook login</a>
			<a href="<?php echo $this->createUrl('/login/twitter') ?>" class="buttons social-twitter ir">Twitter login</a>
			<a href="<?php echo $this->createUrl('/login/googleplus') ?>" class="buttons social-google ir">Google+ login</a>
		</div>
		<div class="use-email">
			<p>Ou, cadastre-se usando seu e-mail:</p>

			<form action="<?php echo $this->createUrl('/register/go') ?>" method="POST" name="register" data-message="Preencha todos os campos" data-validate-email="true">
				<fieldset>
	                <label>Nome completo:</label>
	                <input type="text" name="User[name]" class="default-field name">
	            </fieldset>
	            <fieldset>
	                <label>E-mail:</label>
	                <input type="text" name="User[email]" class="default-field email" data-message="Digite um e-mail válido">
	            </fieldset>
	            <fieldset>
	            	<div class="clearfix">
	            		<div class="fl">
	            			<label>Senha:</label>
	                		<input type="password" name="User[password]" class="default-field password" data-message="As senhas não conferem">
	            		</div>
	            		<div class="fr">
	            			<label>Confirme sua Senha:</label>
	                		<input type="password" name="User[passwordConfirm]" class="default-field check-password" >
	            		</div>
	            	</div>
	            </fieldset>
	            <fieldset>
	            	<p class="terms">Ao clicar em "cadastrar" você declara ter lido e concordado com os <a href="<?php echo $this->createUrl('/sobre/termos-de-uso') ?>">termos de uso</a>.</p>
	            </fieldset>
				<fieldset class="ar">
					<div class="error hide"><p></p></div>
					<input type="submit" value="CADASTRAR" class="default-button">
				</fieldset>
			</form>
		</div>
	</div>
</div>