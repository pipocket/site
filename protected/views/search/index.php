<?php
	$this->cs->registerScript('home2', 'filter(true); organize();');
?>

<div class="filter">
	<div class="area">
		<div class="bar"></div>
		<div class="bar continuous"></div>
		<a href="#" class="pin"></a>

		<ul class="unstyled clearfix">
			<li>
				<a href="<?php echo $this->createUrl('/estreias') ?>" class="estreias first <?php echo $currentFilter=='estreias' ? 'active' : '' ?>" data-position="10" data-drag-limit="40" data-slug="estreias">Estréias</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('/melhores-avaliados') ?>" class="melhores-avaliados <?php echo $currentFilter=='melhores-avaliados' ? 'active' : '' ?>" data-position="130" data-drag-limit="180" data-slug="melhores-avaliados">Melhores avaliados</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('/mais-vistos') ?>" class="mais-vistos <?php echo $currentFilter=='mais-vistos' ? 'active' : '' ?>" data-position="260" data-drag-limit="300" data-slug="mais-vistos">Mais vistos</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('/em-cartaz') ?>" class="em-cartaz <?php echo $currentFilter=='em-cartaz' ? 'active' : '' ?>" data-position="360" data-drag-limit="385" data-slug="em-cartaz">Em cartaz</a>
			</li>
			<li>
				<a href="<?php echo $this->createUrl('/todos') ?>" class="todos last <?php echo $currentFilter=='todos' ? 'active' : '' ?>" data-position="443" data-drag-limit="" data-slug="todos">Todos</a>
			</li>
		</ul>
	</div>
</div>

<div class="main-area">
	<h2>BUSCA POR FILMES</h2>

	<div class="main search">
		<div class="grid grid-search">
			<?php if ($movies): ?>
			<?php echo $movies ?>
			<?php else: ?>	
			<p>Não foram encontrados resultados para a palavra digitada</p>
			<?php endif ?>
		</div>
	</div>
</div>

<?php if ($cast): ?>
<div class="main-area search">
	<h2>ATORES E DIRETORES</h2>

	<div class="main">
		<div class="simple-grid clearfix">
			<?php echo $cast ?>
		</div>
	</div>
</div>
<?php endif; ?>