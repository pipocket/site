<ul class="unstyled">
    <?php foreach ($data as $type => $items): ?>
        <?php if (count($items)>0): ?>
        <li>
            <span class="line"></span>
            <?php if ($type=='movies'): ?>
            <label>Filmes</label>
            <?php elseif($type=='cast'): ?>
            <label>Atores e Diretores</label>
            <?php endif ?>
        </li>
        <?php endif ?>
        <?php foreach ($items as $item): ?>
        <li>
            <a href="<?php echo $this->createUrl('/filme/'.$item['slug']) ?>" class="clearfix">
                <span class="image">
                    <?php if ($item['image']): ?>
                    <img src="<?php echo Yii::app()->params['aws']['s3BaseUrl'] . $this->getImage($item['image'], 'small') ?>" width="35" height="53">
                    <?php endif ?>
                </span>
                <span class="info">
                    <?php echo $item['name'] ?><br/>
                    <?php echo $item['additional']; ?>
                </span>
            </a>
        </li>
        <?php endforeach ?>
    <?php endforeach ?>
</ul>