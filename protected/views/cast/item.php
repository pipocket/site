<?php foreach ($items as $i=>$person): ?>
<div class="box">
	<div class="cast">
		<?php if ($person->photo): ?>
		<img src="<?php echo Yii::app()->params['aws']['s3BaseUrl'] . $this->getImage($person->photo, 'medium') ?>" alt="">
		<?php endif ?>
		<label><?php echo $person->name ?></label>
	</div>
</div>
<?php endforeach ?>