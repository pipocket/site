<div class="main-area">
	<h2>Termos de uso</h2>

	<div class="clearfix">
		<div class="main about">
			<p>Este documento define os termos de uso do site Pipocket.</p>
			<p>O Site tem como propósito ajudar pessoas a escolher, avaliar, e compartilhar opiniões sobre filmes.</p>
		</div>
		<div class="sidebar about">
			<?php echo $this->renderPartial('menu', array('active'=>'terms')) ?>
		</div>
	</div>
</div>