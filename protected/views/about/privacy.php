<div class="main-area">
	<h2>Política de Privacidade</h2>

	<div class="clearfix">
		<div class="main about">
			<p>Informações e atividades do usuário são coletadas para medir o uso de nossos serviços e assim melhorá-los ao longo do tempo.</p>
			<p><strong>Dados pessoais</strong></p>
			<p>Ao fazer parte do Pipocket algumas informações pessoais são fornecidas, algumas como Nome, link para Perfil do Facebook ou Twitter, são exibidas publicamente pelos nossos serviços, salvo no quando o usuário opta por não exibir tais informações.</p>
			<p><strong>Dados adicionais</strong></p>
			<p>O usuário pode fornecer informações adicionais após o cadastro, como sexo, cidade e país onde vive. O sexo é usado apenas para fins de estatísticas, e o local onde vive é exibido em seu perfil somente quando solicitado.</p>
			<p><strong>Serviços de Terceiros</strong></p>
			<p>O Pipocket é integrado a algumas redes sociais, tal postura serve para facilitar e/ou entreter os usuários do site.</p>
			<p><strong>Twitter</strong></p>
			<p>O usuário poderá usá-lo como meio de login, e compatilhar informações do Pipocket em seu perfil do Twitter.</p>
			<p><strong>Facebook</strong></p>
			<p>O usuário poderá usá-lo como meio de login, e compartilhar indormações do Pipocket em seu perfil do Facebook.</p>
		</div>

		<div class="sidebar about">
			<?php echo $this->renderPartial('menu', array('active'=>'privacy')) ?>
		</div>
	</div>
</div>