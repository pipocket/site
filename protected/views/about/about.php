<div class="main-area">
	<h2>Sobre o Pipocket</h2>

	<div class="clearfix">
		<div class="main about">
			<p>Pipocket é um aplicativo que tem como objetivo criar um ranking sólido de filmes em cartaz, baseado nas avaliações de usuários de todo o Brasil.</p>
			<p>A proposta é simples, o usuário compartilha sua opinião sobre o filme, dando uma nota e um comentário.</p>
			<p><strong>Regras do ranking</strong></p>
			<p>O filme permanece no ranking enquanto estiver dentro do intervalo de 2 semanas contando a data de estréia, e a partir do momento em que atinge o número mínimo de votos, este determinado pela média aritmética entre número total de votos e número total de filmes presentes dentro do intervalo de ranking.</p>
		</div>

		<div class="sidebar about">
			<?php echo $this->renderPartial('menu', array('active'=>'about')) ?>
		</div>
	</div>
</div>