<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
        <meta name="description" content="Avaliação de filmes">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="<?php echo $this->baseUrl ?>/public/css/normalize.min.css">
        <link rel="stylesheet" href="<?php echo $this->baseUrl ?>/public/css/main.<?php echo Yii::app()->params['env']=='dev' ? 'css' : 'min.css' ?>">
        <link href='http://fonts.googleapis.com/css?family=News+Cycle:400,700|PT+Sans:400,700' rel='stylesheet' type='text/css'>
        <script src="<?php echo $this->baseUrl ?>/public/js/libs.min.js"></script>
        <script>var basePath = "<?php echo $this->createUrl('/') ?>";</script>
         <!--[if lt IE 9]><script src="<?php echo $this->baseUrl ?>/public/js/plugins/css3-mediaqueries.js"></script><![endif]-->
    </head>
    <body>
        <div id="header">
            <div class="container clearfix">
                <h1>
                    <a href="<?php echo $this->createAbsoluteUrl('/') ?>"><img src="<?php echo $this->baseUrl ?>/public/images/logo.png" alt="Pipocket" width="130"></a>
                </h1>

                <a href="#" class="toggle-mobile-menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <ul class="unstyled mobile-menu">
                    <li><a href="<?php echo $this->createUrl('/estreias') ?>">Estréias</a></li>
                    <li><a href="<?php echo $this->createUrl('/melhores-avaliados') ?>">Melhores avaliados</a></li>
                    <li><a href="<?php echo $this->createUrl('/mais-vistos') ?>">Mais vistos</a></li>
                    <li><a href="<?php echo $this->createUrl('/em-cartaz') ?>">Em cartaz</a></li>
                    <li><a href="<?php echo $this->createUrl('/todos') ?>">Todos</a></li>
                </ul>

                <ul class="menu unstyled clearfix">
                    <?php if (!Yii::app()->user->isGuest): ?>
                    <li class="user clearfix">
                        <a href="#" class="overlay principal"><span class="icons arrow-down"></span></a>

                        <span class="username">
                            <span class="welcome">Seja Bem-vindo</span>
                            <span class="name"><?php echo Yii::app()->user->name ?></span>
                        </span>
                        <span class="photo">
                            <span class="photo-wrap">
                                <img src="<?php echo Yii::app()->user->avatar ?>" alt="">
                            </span>
                        </span>

                         <ul class="submenu hidden">
                            <li class="header"></li>
                            <li class="ttl">Meu Perfil</li>
                            <li><a href="#">Meu perfil</a></li>
                            <li><a href="#">Meus comentários</a></li>
                            <li><a href="#">Meus filmes</a></li>
                            <li><a href="#">Configurações da conta</a></li>
                            <li><a href="<?php echo $this->createUrl('/logout') ?>">Sair</a></li>
                        </ul>
                    </li>
                    <li class="icons separator"></li>
                    <li class="notify">
                        <a href="#" class="icons principal"><!-- <span class="icons">3</span> --></a>

                        <ul class="submenu hidden">
                            <li class="header"></li>
                            <li class="ttl">Notificações</li>
                            <li class="item clearfix new">
                                <span class="photo"><img src="<?php echo $this->baseUrl ?>/public/images/tmp-user-header.jpg" width="55"></span>
                                <span class="info">
                                    <label class="user">Thaís de Oliveira</label> respondeu seu comentário em <label class="movie">Homem de Ferro 3</label>
                                    <span class="date">3 em 21/01/2013</span>
                                </span>

                                <a href="#" class="overlay"></a>
                            </li>
                            <li class="item clearfix">
                                <span class="photo"><img src="<?php echo $this->baseUrl ?>/public/images/tmp-user-header.jpg" width="55"></span>
                                <span class="info">
                                    <label class="user">Thaís de Oliveira</label> respondeu seu comentário em <label class="movie">Homem de Ferro 3</label>
                                    <span class="date">3 em 21/01/2013</span>
                                </span>

                                <a href="#" class="overlay"></a>
                            </li>
                            <li class="all">
                                <a href="#">Ver todos</a>
                            </li>
                        </ul>
                    </li>
                    <?php else: ?>
                    <li><a href="<?php echo $this->createUrl('/cadastro') ?>" class="last">CADASTRE-SE</a></li>
                    <li class="icons separator"></li>
                    <li><a href="#" class="md-trigger" data-modal="login-register">ENTRAR</a></li>
                    <?php endif ?>
                    <li class="icons separator separator-about"></li>
                    <li class="about">
                        <a href="#" class="icons principal">SOBRE</a>

                        <!-- Submenu -->
                        <ul class="unstyled submenu hidden">
                            <li class="header"></li>
                            <li class="ttl">Sobre</li>
                            <li><a href="<?php echo $this->createUrl('/sobre') ?>">Sobre o Pipocket</a></li>
                            <li><a href="<?php echo $this->createUrl('/sobre/politica-de-privacidade') ?>">Política de Privacidade</a></li>
                            <li><a href="<?php echo $this->createUrl('/sobre/termos-de-uso') ?>">Termos de Uso</a></li>
                            <li><a href="<?php echo $this->createUrl('/contato') ?>">Contato</a></li>
                            <li><a href="http://www.facebook.com/pipocket" rel="external">Facebook</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

        <div id="container">
            <?php if ($this->id != 'register' AND $this->id != 'login'): ?>
            <div id="search">
                <form action="<?php echo $this->createUrl('/busca') ?>" method="get">
                    <div class="wrap">
                        <input type="text" class="field" value="<?php echo Yii::app()->request->getParam('term') ?>" data-default-value="BUSCAR FILMES, ATORES E DIRETORES">
                        <input type="submit" class="btn icons" value="">
                        <div class="auto-complete"></div>
                    </div>
                </form>
            </div>
            <?php endif ?>

            <?php if (Yii::app()->user->hasFlash('register')): ?>
                <?php $this->renderPartial('//common/message', array('type'=>'register', 'status'=>Yii::app()->user->getFlash('register'))) ?>
            <?php endif ?>

            <div id="content" class="area-<?php echo $this->id ?>">
                <?php if ($this->currentFilterName): ?>
                <div class="mobile-page-title">
                    <h2><?php echo $this->currentFilterName ?></h2>
                    <hr>
                </div>
                <?php endif ?>

                <?php echo $content ?>
            </div>
        </div>

        <?php if ($this->id!='home'): ?>
        <div id="footer">
            <div class="container clearfix">
                <div class="left">
                    <p>Pipocket &copy; 2011 - 2013.<br/>Todos os direitos reservados. </p>
                </div>
                <div class="right clearfix">
                    <a href="#" class="twitter icons ir" title="@pipocketOficial">Twitter</a>
                    <a href="#" class="youtube icons ir" title="Youtube do Pipocket">Youtube</a>
                    <a href="#" class="facebook icons ir" title="facebook.com/pipocket">Facebook</a>
                </div>
            </div>
        </div>    
        <?php endif ?>

        <!-- Modals -->
        <?php $this->renderPartial('//login/modal') ?>
        <?php $this->renderPartial('//movie/modal') ?>

        <!-- Call scripts -->
        <?php if (Yii::app()->params['env'] == 'dev'): ?>
        <script src="<?php echo $this->baseUrl ?>/public/js/plugins/plugins.js"></script>    
        <?php endif ?>
        <script src="<?php echo $this->baseUrl ?>/public/js/main.<?php echo Yii::app()->params['env']=='dev' ? 'js' : 'min.js' ?>"></script>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        <!-- Plus One -->
        <script type="text/javascript">
          window.___gcfg = {lang: 'pt-BR'};
          (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
          })();
        </script>
        <!-- Analytics -->
        <?php if (Yii::app()->params['analytics']): ?>
        <script>
            var _gaq=[['_setAccount','UA-29570882-1'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
        <?php endif ?>
    </body>
</html>