<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	public function authenticate()
	{
		// Form login
		if (!$this->socialLogin) {
			$user = User::model()->findByAttributes( array('email'=>$this->username) );
		}
		// Social Login
		else {
			$criteria = new CDbCriteria;
			$criteria->compare('network', $this->socialLogin);
			$criteria->compare('network_id', $this->username);

			$user = User::model()->with('social')->find($criteria);
		}

		if ($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif (!Bcrypt::verify($this->password, $user->password) AND !$this->socialLogin)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		elseif ($user->status==0)
			$this->errorCode=self::ERROR_INACTIVE_ACCOUNT;
		elseif ($user->status==2)
			$this->errorCode=self::ERROR_BLOCKED_ACCOUNT;
		else {
			// Controller
			$controller = Yii::app()->controller;

			// Add data on Yii:app()->user
			$this->setState('name', $user->name);
			$this->setState('avatar', ($user->avatar ? $user->avatar : $controller->createAbsoluteUrl('/public/images/tmp-user-header.jpg')));

			$this->errorCode=self::ERROR_NONE;
		}

		return $this->errorCode;
	}
}