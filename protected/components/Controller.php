<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	/*
	* Manipulated vars
	*/
	public $baseUrl;
	public $cs;
	public $currentFilterName;

	public function init()
	{
		Yii::app()->language = 'pt-br';
		$this->basic();
	}

	public function basic()
	{
		$this->baseUrl = Yii::app()->request->baseUrl;
		$this->cs = Yii::app()->clientScript;

		// Disable autoload of jquery 
		$this->cs->scriptMap['jquery.js'] = false;
	}

	public function jsonResponse($data, $jsonp=false)
	{
		$data = CJavaScript::jsonEncode( $data );

		if ($jsonp) {
			$data = Yii::app()->request->getParam('callback') . "([{$data}])";
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo $data;
	}

	public function getImage($image, $length='')
	{
		return $length ? str_replace('.', "-{$length}.", $image) : $image;
	}

	public function daysLeft($date)
	{
		if ($date) {
			$response = 'Estréia ';

			$date = strtotime($date);
			$diff = $date-strtotime(date('Y-m-d'));
			$diff = $diff/(60*60*24);
			
			if($diff==1)
				$response .= '<strong>Amanhã</strong>';
			elseif($diff==0)
				$response .= '<strong>Hoje</strong>';
			else
				$response .= 'em <strong>' . Yii::app()->dateFormatter->format('d \'de\' MMMM', $date) . '</strong>';
		}
		else {
			$response = 'Data de estréia não confirmada';
		}

		return $response;
	}

	public function whenOccurred($date)
	{
		$date = strtotime(date('Y-m-d', strtotime($date)));
		$diff = strtotime(date('Y-m-d'))-$date;
		$diff = $diff/(60*60*24);
		$message = null;

		switch ($diff) {
			case 0:
				$message = 'Avaliou hoje';
				break;

			default:
				$message = 'Avaliou há '.$diff.' dias';
				break;
		}

		return $message;
	}

	public function terms($term, $value)
	{
		$data = '';

		$terms = array(
			'favorite' => array('', '{value} Favoritou', '{value} Favoritaram'),
			'comment' => array('', '{value} Comentou', '{value} Comentaram'),
			'evaluatedBy' => array('', 'Avaliado por <strong>{value} pessoa</strong>', 'Avaliado por <strong>{value} pessoas</strong>'),
			'attended' => array('', 'pessoa assistiu', 'pessoas assistiram'),
			'wantToWatch' => array('', '{value} Quer assistir', '{value} Querem assistir'),
		);

		if (is_numeric($value) AND isset($terms[$term])) {
			if ($value>1) {
				$data = $terms[$term][2];
			}
			else if ($value==1) {
				$data = $terms[$term][1];
			}
			else {
				$data = $terms[$term][0];
			}

			$data = str_replace('{value}', $value, $data);
		}

		return $data;
	}

	protected function generateUserCode($userId, $userEmail)
	{
		return base64_encode($userId) . Yii::app()->params['securityResetPasswordHash'] . Bcrypt::hash($userEmail);
	}

	protected function decodeUserCode($code)
	{
		$data = array();

		$split = explode(Yii::app()->params['securityResetPasswordHash'], $code);

		if (count($split)==2) {
			$data['id'] = base64_decode(reset($split));
			$data['email'] = array_pop($split);
		}

		return (object)$data;
	}

	protected function onlyGuest()
	{
		if (!Yii::app()->user->isGuest) {
			$this->redirect( $this->createAbsoluteUrl('/') );
		}
	}

	protected function dateFormat($date, $format='dd/MM/y')
	{
		return Yii::app()->dateFormatter->format($format, strtotime($date));
	}
}