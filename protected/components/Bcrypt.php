<?php
/**
 * Bcrypt hashing class
 * 
 * @author Paulo Martins <phmartins6@gmail.com>
 */
class Bcrypt extends CComponent
{
	protected static $saltPrefix = '2a';
	protected static $defaultCost = 8;
	protected static $saltLength = 22;

	/**
	 * Hash a string
	 * 
	 * @param  string  $string The string
	 * 
	 * @see    http://www.php.net/manual/en/function.crypt.php
	 * 
	 * @return string
	 */
	static function hash($string)
	{
		return crypt($string, self::_hashString());
	}

	/**
	 * Check a hashed string
	 * 
	 * @param  string $string The string
	 * @param  string $hash   The hash
	 * 
	 * @return boolean
	 */
	public static function verify($string, $hash)
	{
		return (crypt($string, $hash) === $hash);
	}

	/**
	* Generate a random base64 encoded salt
	* 
	* @return string
	*/
	public static function randomSalt()
	{
		$seed = uniqid(mt_rand(), true);

		$salt = base64_encode($seed);
		$salt = str_replace('+', '.', $salt);

		return substr($salt, 0, self::$saltLength);
	}

	private static function _hashString()
	{
		return sprintf('$%s$%02d$%s$', self::$saltPrefix, self::$defaultCost, self::randomSalt());
	}
}