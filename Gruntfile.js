module.exports = function(grunt) {
	'use strict';

	var gruntConfig = {
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				files: {
					'public/css/main.css': 'public/src/main.scss'
				}
			}
		},
		coffee: {
			compile: {
				files: {
					'public/js/main.js': 'public/src/main.coffee'
				}
			}
		},
		watch: {
			// css: {
			// 	files: 'public/src/*.scss',
			// 	tasks: ['sass']
			// },
			scripts: {
				files: 'public/src/*.coffee',
				tasks: ['coffee']
			}
		},
		cssmin: {
			banner: {
				files: {
					'public/css/main.min.css': ['public/css/normalize.min.css', 'public/css/main.css']
				}
			}
		},
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
			},
			pre: {
				files: {
					'public/js/libs.min.js': ['bower_components/modernizr/modernizr.js', 'bower_components/jquery/jquery.js']
				}
			},
			target: {
				files: {
					'public/js/main.min.js': ['public/js/plugins/plugins.js', 'public/js/main.js']
				}
			}
		},
		jshint: {
			all: ['public/js/main.js']
		}
	}

	grunt.initConfig(gruntConfig);

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-coffee');

	grunt.registerTask('default', ['uglify:pre', 'watch']);
	grunt.registerTask('golive', ['uglify', 'cssmin']);
}